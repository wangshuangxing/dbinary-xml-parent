package com.binary.exception;

public class BusinessException extends BaseException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BusinessException(ExceptionEnum enums){
		
		super(enums);
		
	}
	public BusinessException(String message){
		
		super(message);
		
	}
}

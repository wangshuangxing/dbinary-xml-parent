package com.binary.exception;

public class BaseException extends RuntimeException {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// 友好的code
	protected int friendlyCode;

	// 友好的message
	protected String friendlyMsg;

	// 有好的url
	protected String urlPath;
	public BaseException(String message){
		super(message);
		this.value(500, message, "");
	}
	public BaseException(ExceptionEnum enums){
		
		this.value(enums.getCode(), enums.getMsg(), enums.getPathUrl());
	}
	public BaseException(int friendlyCode, String friendlyMsg, String urlPath) {
		super();
		this.friendlyCode = friendlyCode;
		this.friendlyMsg = friendlyMsg;
		this.urlPath = urlPath;
	}
	public void  value(int friendlyCode, String friendlyMsg, String urlPath) {
		this.friendlyCode = friendlyCode;
		this.friendlyMsg = friendlyMsg;
		this.urlPath = urlPath;
	}
	public int getFriendlyCode() {
		return friendlyCode;
	}


	public void setFriendlyCode(int friendlyCode) {
		this.friendlyCode = friendlyCode;
	}


	public String getFriendlyMsg() {
		return friendlyMsg;
	}


	public void setFriendlyMsg(String friendlyMsg) {
		this.friendlyMsg = friendlyMsg;
	}


	public String getUrlPath() {
		return urlPath;
	}


	public void setUrlPath(String urlPath) {
		this.urlPath = urlPath;
	}


	
	
	
	

}

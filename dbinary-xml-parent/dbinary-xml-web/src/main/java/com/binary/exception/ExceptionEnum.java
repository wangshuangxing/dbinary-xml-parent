package com.binary.exception;

public enum ExceptionEnum {

	INNER500URLEXCEPTION(500,"业务异常",""),
	INNER500EXCEPTION(500,"业务异常");
	private ExceptionEnum(int code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	
	private ExceptionEnum(int code, String msg, String pathUrl) {
		this.code = code;
		this.msg = msg;
		this.pathUrl = pathUrl;
	}


	// �Ѻ���ʾ��code��
	protected int code;

	// �Ѻ���ʾ
	protected String msg;
	//��תURL
	protected String pathUrl;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getPathUrl() {
		return pathUrl;
	}

	public void setPathUrl(String pathUrl) {
		this.pathUrl = pathUrl;
	}

	

	
}

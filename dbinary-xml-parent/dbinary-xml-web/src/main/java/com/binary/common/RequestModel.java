package com.binary.common;

import java.io.Serializable;
import java.util.List;

import com.binary.vo.BaseVo;

public class RequestModel<T extends BaseVo> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<T> requestList;
	private PageVo pageVo;
	
	
	public PageVo getPageVo() {
		return pageVo;
	}
	public void setPageVo(PageVo pageVo) {
		this.pageVo = pageVo;
	}
	public List<T> getRequestList() {
		return requestList;
	}
	public void setRequestList(List<T> requestList) {
		this.requestList = requestList;
	}

	
	
}

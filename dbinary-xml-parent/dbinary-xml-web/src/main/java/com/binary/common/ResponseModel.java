package com.binary.common;

import java.io.Serializable;
import java.util.List;

import com.binary.vo.BaseVo;

/**
 * @author wangchangbo
 *
 * @param <T>
 */
public class ResponseModel<T extends BaseVo> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<T> resultList;
	private PageVo pageVo;
	private Message message;
	
	
	
	public Message getMessage() {
		return message;
	}
	public void setMessage(Message message) {
		this.message = message;
	}
	public PageVo getPageVo() {
		return pageVo;
	}
	public void setPageVo(PageVo pageVo) {
		this.pageVo = pageVo;
	}
	public List<T> getResultList() {
		return resultList;
	}
	public void setResultList(List<T> resultList) {
		this.resultList = resultList;
	}
	
	
	
}

package com.binary.common;


import java.io.Serializable;

import com.binary.vo.BaseVo;

public class SingleRequestModel<T extends BaseVo> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private T request;
	private PageVo pageVo;
	
	
	public PageVo getPageVo() {
		return pageVo;
	}
	public void setPageVo(PageVo pageVo) {
		this.pageVo = pageVo;
	}
	public T getRequest() {
		return request;
	}
	public void setRequest(T request) {
		this.request = request;
	}
	
	
}

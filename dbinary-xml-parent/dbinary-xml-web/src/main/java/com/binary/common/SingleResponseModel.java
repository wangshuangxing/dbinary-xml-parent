package com.binary.common;


import java.io.Serializable;

import com.binary.vo.BaseVo;

public class SingleResponseModel<T extends BaseVo> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private T result;
	private PageVo pageVo;
	private Message  message;
	
	
	
	public Message getMessage() {
		return message;
	}
	public void setMessage(Message message) {
		this.message = message;
	}
	public PageVo getPageVo() {
		return pageVo;
	}
	public void setPageVo(PageVo pageVo) {
		this.pageVo = pageVo;
	}
	public T getResult() {
		return result;
	}
	public void setResult(T result) {
		this.result = result;
	}
	
	
	
}

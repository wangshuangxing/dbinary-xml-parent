package com.binary.enums;

public enum SuccessOrFailEnum {

	SUCCESS("success","成功"),
	FAIL("fail","失败");
	private String code;
	private String message;
	
	private SuccessOrFailEnum(String code, String message) {
		this.code = code;
		this.message = message;
	}
	public static SuccessOrFailEnum getEnumByCode(String code){
		SuccessOrFailEnum enum0 = null;
		if(code!=null&&!"".equals(code)){
			for(SuccessOrFailEnum enum1:SuccessOrFailEnum.values()){
				if(enum1.getCode().equals(code)){
					enum0 = enum1;
					break;
				}
			}
			return enum0;
		}
		return enum0;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}

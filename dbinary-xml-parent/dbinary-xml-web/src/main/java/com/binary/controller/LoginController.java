package com.binary.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.binary.common.Message;
import com.binary.common.SingleResponseModel;
import com.binary.enums.SuccessOrFailEnum;
import com.binary.exception.BusinessException;
import com.binary.vo.UserVo;

@Controller
@RequestMapping("login")
public class LoginController {
	private final static Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	
	@RequestMapping(value="/login",method = RequestMethod.POST)
	@ResponseBody
	public SingleResponseModel<?> login(UserVo vo){
		SingleResponseModel<UserVo> userVoModel = new SingleResponseModel<UserVo>();
		logger.info("登录的账号{} ，密码{}",vo.getUsername(),vo.getPassword());
		if(vo.getUsername()==null||vo.getPassword()==null||"".equals(vo.getUsername())||"".equals(vo.getPassword())){
			throw new BusinessException("账号密码为空，请查看再次输入，谢谢！！！！");
		}
		userVoModel.setResult(vo);
		Message message = new Message();
		message.setCode(SuccessOrFailEnum.SUCCESS.getCode());
		message.setMessage(SuccessOrFailEnum.SUCCESS.getMessage());
		userVoModel.setMessage(message);
		return userVoModel;
		
	}
	
	

}

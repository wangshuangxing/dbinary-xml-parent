package com.binary.controller;


import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.binary.common.Message;
import com.binary.common.ResponseModel;
import com.binary.entity.UserRole;
import com.binary.enums.SuccessOrFailEnum;
import com.binary.service.UserRoleService;
import com.binary.vo.UserRoleVo;

@Controller
@RequestMapping("userRole")
public class UserRoleController {
	@SuppressWarnings("unused")
	private final static Logger logger = LoggerFactory.getLogger(UserRoleController.class);
	@Autowired
	private UserRoleService userRoleService;
	
	@RequestMapping(value="/getUserRoleInfo",method = RequestMethod.GET)
	@ResponseBody
	public ResponseModel<?> getUserRoleInfo(String userName){
		ResponseModel<UserRoleVo> userVoModel = new ResponseModel<UserRoleVo>();
		List<UserRole> list = userRoleService.getUserRoleInfo(userName);
		List<UserRoleVo> lists = new ArrayList<UserRoleVo>(); 
		Message message = new Message();
		if(!list.isEmpty()&&list.size()>0){
			for(UserRole ur:list){
				UserRoleVo vo = new UserRoleVo();
				BeanUtils.copyProperties(ur, vo);
				lists.add(vo);
			}
			message.setCode(SuccessOrFailEnum.SUCCESS.getCode());
			message.setMessage(SuccessOrFailEnum.SUCCESS.getMessage());
		}else{
			message.setCode(SuccessOrFailEnum.FAIL.getCode());
			message.setMessage(SuccessOrFailEnum.FAIL.getMessage());
		}
		userVoModel.setResultList(lists);
		userVoModel.setMessage(message);
		return userVoModel;
		
	}
	
	

}

/**
 * 
 */
package com.binary.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.binary.entity.FacadeFactory;
import com.binary.service.CentralServiceFactory;
import com.binary.xml.Body;
import com.binary.xml.UserInfo;

/**
 * @author dbinary
 * @date 2018年8月10日 下午10:21:22
 */
@Controller
@RequestMapping("test")
public class CentralFacadeController {
	@Autowired
	private CentralServiceFactory centralService;
	@RequestMapping("/testHttp")
	@ResponseBody
	public Object sendUserInfo(){
		UserInfo userInfo = new UserInfo();
		userInfo.setCode("c_wangchangbo-001");
		userInfo.setName("王昌博");
		userInfo.setGender("男");
		FacadeFactory<Body> result = centralService.getUserInfo(userInfo);
		return result;
	}
}

package com.binary.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.binary.annotations.BusinessLogger;
import com.binary.common.Message;
import com.binary.common.ResponseModel;
import com.binary.common.SingleResponseModel;
import com.binary.constant.RedisConstant;
import com.binary.entity.FacadeFactory;
import com.binary.entity.Role;
import com.binary.enums.SuccessOrFailEnum;
import com.binary.exception.BusinessException;
import com.binary.exception.ExceptionEnum;
import com.binary.redis.DataRedisClient;
import com.binary.service.CentralServiceFactory;
import com.binary.service.RoleService;
import com.binary.vo.RoleVo;
import com.binary.xml.Body;
import com.binary.xml.UserInfo;

@Controller
@RequestMapping("role")
public class RoleController {
	@Autowired
	private RoleService roleService;


	@Resource(name = "dataRedisClient")
	private DataRedisClient redisClient;
	private final static Logger logger = LoggerFactory.getLogger(RoleController.class);

	@RequestMapping(value = "/getRoleInfo", method = RequestMethod.GET)
	@ResponseBody
	public SingleResponseModel<?> getRoleInfo(@RequestParam("roleName") String roleName) {

		SingleResponseModel<RoleVo> roleVoModel = new SingleResponseModel<RoleVo>();
		try {
			
			Role role = redisClient.getJsonOneResult(1, RedisConstant.RoleKey_ + roleName, Role.class);
			RoleVo roleVo = new RoleVo();
			if (role != null) {
				BeanUtils.copyProperties(role, roleVo);
				roleVoModel.setResult(roleVo);
				Message message = new Message();
				message.setCode(SuccessOrFailEnum.getEnumByCode("success").getCode());
				message.setMessage(SuccessOrFailEnum.getEnumByCode("success").getMessage());
				roleVoModel.setMessage(message);
				logger.info("jedis get value :{}", JSON.toJSONString(role));
				return roleVoModel;
			}
			role = roleService.getRoleInfo(roleName);
			if (role != null) {
				BeanUtils.copyProperties(role, roleVo);
				roleVoModel.setResult(roleVo);
			} else {
				roleVoModel.setResult(null);
			}
			Message message = new Message();
			message.setCode(SuccessOrFailEnum.getEnumByCode("success").getCode());
			message.setMessage(SuccessOrFailEnum.getEnumByCode("success").getMessage());
			roleVoModel.setMessage(message);
			if (role != null) {
				redisClient.set(1, RedisConstant.RoleKey_ + roleName, role);
				logger.info("jedis set value :{}", JSON.toJSONString(role));
			}
		} catch (Exception e) {
			logger.error("错误:" + e.getMessage(), e);
			throw new BusinessException(ExceptionEnum.INNER500EXCEPTION);
		}
		return roleVoModel;
	}

	@BusinessLogger(value = "多个查询", key = "roleName")
	@RequestMapping(value = "/getRoleInfos.do", method = RequestMethod.GET)
	@ResponseBody
	public ResponseModel<?> getRoleInfoOther(@RequestParam(value = "roleName", required = false) String roleName) {

		ResponseModel<RoleVo> roleVoModel = new ResponseModel<RoleVo>();
		Message message = new Message();
		RoleVo roleVo = null;
		try {
			List<Role> roles = roleService.getListRoleInfo(roleName);
			List<RoleVo> roleList = new ArrayList<RoleVo>();
			for (Role role : roles) {
				roleVo = new RoleVo();
				BeanUtils.copyProperties(role, roleVo);
				roleList.add(roleVo);
			}
			roleVoModel.setResultList(roleList);
			message.setCode(SuccessOrFailEnum.getEnumByCode("success").getCode());
			message.setMessage(SuccessOrFailEnum.getEnumByCode("success").getMessage());
			roleVoModel.setMessage(message);
		} catch (Exception e) {
			logger.error("错误:" + e.getMessage(), e);
			throw new BusinessException(ExceptionEnum.INNER500EXCEPTION);
		}

		return roleVoModel;
	}
}

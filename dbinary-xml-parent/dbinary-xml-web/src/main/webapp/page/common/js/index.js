$(function(){
	var responseData;

	$.initContext({
		pageCode:"portal_index",
		dataLoadedCallback: function () {
			initialiseData($.pageContext.dicts);
		},
		ajaxStartCallback: showLoading,
		ajaxCompleteCallback: hideLoading
	});

	function initialiseData(dicts){
		// $.ajax({
		// 	type:"get",
		// 	url: "../../../../login/currentUser?" + new Date().getTime(),
		// 	dataType: "json",
		// 	contentType: 'application/json;charset=utf-8',
		// 	success:function(response){
		// 		$("#loginUser").html(response.userName);
		// 		$('#partnerCode').html(response.partnerCode);
		// 		$('#partnerName').html(response.partnerName);
		// 		$('#agentCode').html(response.agentCode);
		// 		$('#agentName').html(response.agentName);
		// 		$('#inputorCode').html(response.inputorCode);
		// 		$('#inputorName').html(response.inputorName);
		// 		$("#collarForm").attr("action", response.system_url);
		// 		$("#collarForm input:hidden").each(function (i, v) {
		// 			$(v).val(response[$(v).attr("name")]);
		// 		});
		// 		responseData=response;
		// 	},error: function(xmlHttpRequest,textStatus,errorThrow){
		// 		ajaxError( xmlHttpRequest,textStatus,errorThrow );
		// 	}

		// });

		cpic_ajax({
			url: "login/currentUser",
			data:{"meta":{},"redata": {}},
			success:function(response){
				var result = response.result.currentLoginMap;
				$("#loginUser").html(result.userName);
				$('#partnerCode').html(result.partnerCode);
				$('#partnerName').html(result.partnerName);
				$('#agentCode').html(result.agentCode);
				$('#agentName').html(result.agentName);
				$('#inputorCode').html(result.inputorCode);
				$('#inputorName').html(result.inputorName);
				$("#collarForm").attr("action", result.system_url);
				$("#collarForm input:hidden").each(function (i, v) {
					$(v).val(result[$(v).attr("name")]);
				});
				responseData=result;

				cpic_ajax({
					url: "user/queryAuthMenus",
					data:{"meta":{},"redata": {}},
					success:function(response){
						if (response.result) {
							for (var i = 0; i<response.result.length; i++) {
								$("[data='"+response.result[i].code+"']").removeClass("permissions");
							}
						}
						$(".permissions").find("a").attr("href", "javascript:;");
					}
				},false,false);
			}
		},false,true);

		
	}
	
	$('#collar').click(function(){
		$('#collarForm')[0].submit();
	});

});
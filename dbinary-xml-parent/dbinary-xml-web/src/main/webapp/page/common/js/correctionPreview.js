$(function(){
	$.initContext({
		pageCode:"correction_preview",
		ajaxStartCallback: showLoading,
		ajaxCompleteCallback: hideLoading,
		dataLoadedCallback:function(){
			setTime($.pageContext.userConfigs.currentDate);
		}
	});
    
	if(window.location.href.indexOf("#")!=-1){

		cpic_ajax({
			url:"correction/correctionPreview",
			data:{"meta":{},"redata":{endorsementNo:getAnchor()}},
			success:function(response){
				$.each(response.result,function(key,val){
					$("[data='"+ key +"']").html(val);
				});
				var arr = response.result.endorsementMsgs;
				var str = "";
				for(var i = 0; i<arr.length;i++){
					str += "<p style=>"+arr[i]+"</p>";
				}
				
				$("#endorsementMsg").append(str).find("p").css("float","none");

			}
		},true);

	}

});
$(function(){
    getHeader("");

	$.initContext({
		pageCode:"upload_info",
		ajaxStartCallback: showLoading,
		ajaxCompleteCallback: hideLoading,
		dataLoadedCallback:function(){
			setTime($.pageContext.userConfigs.currentDate);
		}
	});

    var dataType = [
		{"code":"01","name":"证件类型"},
		{"code":"02","name":"行驶证"},
		{"code":"03","name":"车辆合格证"},
		{"code":"04","name":"投保申请书"},
		{"code":"05","name":"车船税证明"},
		{"code":"06","name":"其他"}
	];

	var credentialsType = [
		{"code":"01","name":"身份证"},
		{"code":"02","name":"组织机构代码"},
		{"code":"03","name":"税务登记证"},
		{"code":"04","name":"营业执照"},
		{"code":"05","name":"军官证"},
		{"code":"06","name":"法人证书"},
		{"code":"07","name":"护照"},
		{"code":"08","name":"其他"}
	];

	createOption($("#dataType"),dataType);

	$("#dataType").change(function(){
		var value = $(this).val();
		switch(value){
			case "01":
				createOption($("#credentialsType"),credentialsType);
				break;
			default:
				createOption($("#credentialsType"),[]);
		}
	}); 
});
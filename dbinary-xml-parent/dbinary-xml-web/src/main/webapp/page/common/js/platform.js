/**
 * @author DaoXiang
 */

$(function(){

	$.initContext({
		pageCode:"platform_search",
		ajaxStartCallback: showLoading,
		ajaxCompleteCallback: hideLoading,
		dataLoadedCallback:function(){
			setTime($.pageContext.userConfigs.currentDate);
		}
	});
	
	if(window.location.href.indexOf("#") != -1){
		var params = {
			"meta":{},
			"redata":{quotationNo:getAnchor()}
		};

		cpic_ajax({
			data:params,
			url:"plarformQuery/queryPlatformInfo",
			success:function(response){

				$.each(response.result.platformInfoVo,function(key,value){
					$("#platInfo").find("[data="+key+"]").text(value);
				});

				$.each(response.result.platformInfoVo,function(key,value){
					$("#commercialInfo").find("[data="+key+"]").text(value);
				});
				createTable(response.result.claimInformationVo,$("#claimTable"),$(".claimClone"));
				createTable(response.result.claimInformationSoryVo,$("#claimTrafficTable"),$(".claimTrafficClone"));
				
				createTable(response.result.illegalINformationVO,$("#violateTable"),$(".violateClone"));
				createTable(response.result.plartformCommercialInsuranceVo,$("#commercialTable"),$(".commercialClone"));
				
			}
		},true);
		
	}
	
	$("#close").click(function(){
		window.close();
	});
	
	
});

$(function(){
	$("#j_password").val($("#_password").val());
	
	$("#j_username").focus(function() {
		$("#err").empty();
		if ($("#j_username").val() == "请输入用户名") {
			$("#j_username").val("");
		}
	});
	passwordKey('newPwd');

	$("#_password").focus(function(ev) {
		$("#err").empty();
		if ($("#_password").val() == "请输入密码") {
			$("#_password").val("");
		}
		if($("#_password").val() == "*******"){
			$("#_password").val($("#j_password").val());
		}
	});

	$("#j_username,#_password,#verifyCode").keydown(function(ev){
		var oEvent = ev || event;
		if(oEvent.keyCode == 13 || oEvent.keyCode == 27){
			if($("#_password").val() != "" && $("#_password").val() != "**********"){
				$("#j_password").val($("#_password").val());
			}
			$("#j_login").click();
		}
	});

	$("#_password").blur(function() {
		if($("#_password").val() != ""){
			$("#j_password").val($("#_password").val());
			$("#_password").val("**********");
		}
	});

	$("#refreshAnthor").click(function(){
		refreshImg();
	});

	$("#pwdSubBtn").click(function(){
		changePwd();
	});

	$("#j_login").click(function(){
		if (!checkOut()) {
			return false;
		}
		
		$("#j_login").attr("disabled",true).text("登录中...");
		$("#err").empty();
		
		var j_password = CryptoJS.SHA256($("#j_password").val()+$("#j_username").val());
		
		$.ajax({
			type:"POST",
			dataType:"text",
			url:"../../../../j_spring_security_check",
			data:"j_password="+j_password+"&j_username="+$('#j_username').val()+"&verify_code="+$('#verifyCode').val(),
			success: function (data) {
				var ret = $.parseJSON(data);
				
				if (ret.authentication == "true") {
					if (ret.reqUri) {
						window.top.location.href = ret.reqUri;
						return;
					}
				} 
				else {
					var holdPwd = false;
					
					var errCode = ret.errCode;
					var errMsg = "登录失败，请稍候再试！";
					
					if (errCode == "1") {
						errMsg = "用户名或密码错误！";
					}
					if (errCode == "2") {
						errMsg = "用户已被锁定！";
					}
					if (errCode == "3") {
						errMsg = "验证码错误！";
						holdPwd = true;
					}
					if (errCode == "4") {
						errMsg = "密码已过期！";
						modifyInvaidPwd();
					}
					
					$("#err").append(errMsg);
					
					refreshImg();

					$('#verifyCode').val("");
					
					if (!holdPwd) {
						$("#_password").val("");
						$('#j_password').val("");
					}
				}

			},
			error: function(xmlHttpRequest,textStatus,errorThrow){
				dialogs.error("登录失败，请稍候再试！", errorTittle);
				$("#j_login").attr("disabled",false).text("登录");
			}, 
			complete : function(XMLHttpRequest){
				if(XMLHttpRequest.readyState < 4){
					ajaxReq.abort();
					dialogs.error("请求超时！",errorTittle);
				}
				$("#j_login").attr("disabled",false).text("登录");
			}
		});
	});

	function checkOut() {
		if ($("#_password").val() != "" && $("#_password").val() != "**********") {
			$("#j_password").val($("#_password").val());
		}
		
		if ($("#j_password").val() == "" || $("#j_username").val() == "" || $("#j_username").val() == "请输入用户名"){
			$("#err").empty();
			$("#err").append("用户名、密码不能为空！");
			return false;
		}
		
		if ($("#j_username").val().length > 30 || $("#_password").val().length > 30) {
			$("#err").empty();
			$("#err").append("用户名、密码字符长度不能超过30！");
			return false;
		}
		
		if ($("#verifyCode").val() == ""){
			$("#err").empty();
			$("#err").append("验证码不能为空！");
			return false;
		}
		
		return true;
	}

});

function refreshImg() {
	$("#capImg").attr("src", "../../../../auth/getCaptchaImage?" + Math.random());
}

function modifyInvaidPwd() {
	var titleInfo = "用户 " + $("#j_username").val() + " 的密码已过期，请修改密码！";
	$("#updatePasswordDialog h4").html(titleInfo);

	$("#updatePasswordDialog").show();
}

function changePwd() {
	var username = $("#j_username").val();
	var oldPwd = $("#oldPwd").val();
	var newPwd = $("#newPwd").val();
	var confirmNewPwd = $("#confirmNewPwd").val();

	if (!username) {
		dialogs.warning("用户不存在！", "提示");
		return;
	}
	if (!oldPwd) {
		dialogs.warning("请输入原密码！", "提示");
		return;
	}
	if (!newPwd) {
		dialogs.warning("请输入新密码！", "提示");
		return;
	}
	if (newPwd.length > 30) {
		dialogs.warning("新密码字符长度不能超过30！", "提示");
		return;
	}
	if (oldPwd == newPwd) {
		dialogs.warning("原密码与新密码不能相同！", "提示");
		return;
	}
	if (newPwd != confirmNewPwd) {
		dialogs.warning("两次输入的新密码不一致！", "提示");
		return;
	}

	var flag = isSecurity(newPwd);
	if(flag != 4 && flag != 3){
		dialogs.warning("新密码不符合安全要求！<密码长度需要大于8位，包含数字、小写字母、大写字母或特殊字符>", "提示");
		return;
	}

	$.ajax({
		type: "POST",
		dataType: "json",
		contentType: 'application/json;charset=utf-8',
		url: "../../../../user/changeInvalidPwd",
		data: JSON.stringify({meta: {}, redata: {
			userCode: username,
			oldPwd: CryptoJS.SHA256(oldPwd + username).toString(),
			newPwd: CryptoJS.SHA256(newPwd + username).toString()
		}}),
		success: function (data) {
			if (data.message.code == 'success') {
				$("#updatePasswordDialog").hide();
				$("#err").empty();

				$("#oldPwd").val("");
				$("#newPwd").val("");
				$("#confirmNewPwd").val("");

				refreshImg();
			}
			else {
				dialogs.error(data.message.message, "提示");
			}
		},
		error: function(xmlHttpRequest,textStatus,errorThrow){

		}
	});
}


function SHA256(s){

	var chrsz   = 8;
	var hexcase = 0;

	function safe_add (x, y) {
		var lsw = (x & 0xFFFF) + (y & 0xFFFF);
		var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
		return (msw << 16) | (lsw & 0xFFFF);
	}

	function S (X, n) { return ( X >>> n ) | (X << (32 - n)); }
	function R (X, n) { return ( X >>> n ); }
	function Ch(x, y, z) { return ((x & y) ^ ((~x) & z)); }
	function Maj(x, y, z) { return ((x & y) ^ (x & z) ^ (y & z)); }
	function Sigma0256(x) { return (S(x, 2) ^ S(x, 13) ^ S(x, 22)); }
	function Sigma1256(x) { return (S(x, 6) ^ S(x, 11) ^ S(x, 25)); }
	function Gamma0256(x) { return (S(x, 7) ^ S(x, 18) ^ R(x, 3)); }
	function Gamma1256(x) { return (S(x, 17) ^ S(x, 19) ^ R(x, 10)); }

	function core_sha256 (m, l) {
		var K = new Array(0x428A2F98, 0x71374491, 0xB5C0FBCF, 0xE9B5DBA5, 0x3956C25B, 0x59F111F1, 0x923F82A4, 0xAB1C5ED5, 0xD807AA98, 0x12835B01, 0x243185BE, 0x550C7DC3, 0x72BE5D74, 0x80DEB1FE, 0x9BDC06A7, 0xC19BF174, 0xE49B69C1, 0xEFBE4786, 0xFC19DC6, 0x240CA1CC, 0x2DE92C6F, 0x4A7484AA, 0x5CB0A9DC, 0x76F988DA, 0x983E5152, 0xA831C66D, 0xB00327C8, 0xBF597FC7, 0xC6E00BF3, 0xD5A79147, 0x6CA6351, 0x14292967, 0x27B70A85, 0x2E1B2138, 0x4D2C6DFC, 0x53380D13, 0x650A7354, 0x766A0ABB, 0x81C2C92E, 0x92722C85, 0xA2BFE8A1, 0xA81A664B, 0xC24B8B70, 0xC76C51A3, 0xD192E819, 0xD6990624, 0xF40E3585, 0x106AA070, 0x19A4C116, 0x1E376C08, 0x2748774C, 0x34B0BCB5, 0x391C0CB3, 0x4ED8AA4A, 0x5B9CCA4F, 0x682E6FF3, 0x748F82EE, 0x78A5636F, 0x84C87814, 0x8CC70208, 0x90BEFFFA, 0xA4506CEB, 0xBEF9A3F7, 0xC67178F2);
		var HASH = new Array(0x6A09E667, 0xBB67AE85, 0x3C6EF372, 0xA54FF53A, 0x510E527F, 0x9B05688C, 0x1F83D9AB, 0x5BE0CD19);
		var W = new Array(64);
		var a, b, c, d, e, f, g, h, i, j;
		var T1, T2;

		m[l >> 5] |= 0x80 << (24 - l % 32);
		m[((l + 64 >> 9) << 4) + 15] = l;

		for ( var i = 0; i<m.length; i+=16 ) {
			a = HASH[0];
			b = HASH[1];
			c = HASH[2];
			d = HASH[3];
			e = HASH[4];
			f = HASH[5];
			g = HASH[6];
			h = HASH[7];

			for ( var j = 0; j<64; j++) {
				if (j < 16) W[j] = m[j + i];
				else W[j] = safe_add(safe_add(safe_add(Gamma1256(W[j - 2]), W[j - 7]), Gamma0256(W[j - 15])), W[j - 16]);

				T1 = safe_add(safe_add(safe_add(safe_add(h, Sigma1256(e)), Ch(e, f, g)), K[j]), W[j]);
				T2 = safe_add(Sigma0256(a), Maj(a, b, c));

				h = g;
				g = f;
				f = e;
				e = safe_add(d, T1);
				d = c;
				c = b;
				b = a;
				a = safe_add(T1, T2);
			}

			HASH[0] = safe_add(a, HASH[0]);
			HASH[1] = safe_add(b, HASH[1]);
			HASH[2] = safe_add(c, HASH[2]);
			HASH[3] = safe_add(d, HASH[3]);
			HASH[4] = safe_add(e, HASH[4]);
			HASH[5] = safe_add(f, HASH[5]);
			HASH[6] = safe_add(g, HASH[6]);
			HASH[7] = safe_add(h, HASH[7]);
		}
		return HASH;
	}

	function str2binb (str) {
		var bin = Array();
		var mask = (1 << chrsz) - 1;
		for(var i = 0; i < str.length * chrsz; i += chrsz) {
			bin[i>>5] |= (str.charCodeAt(i / chrsz) & mask) << (24 - i%32);
		}
		return bin;
	}

	function Utf8Encode(string) {
		string = string.replace(/\r\n/g,"\n");
		var utftext = "";

		for (var n = 0; n < string.length; n++) {

			var c = string.charCodeAt(n);

			if (c < 128) {
				utftext += String.fromCharCode(c);
			}
			else if((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}

		}

		return utftext;
	}

	function binb2hex (binarray) {
		var hex_tab = hexcase ? "0123456789ABCDEF" : "0123456789abcdef";
		var str = "";
		for(var i = 0; i < binarray.length * 4; i++) {
			str += hex_tab.charAt((binarray[i>>2] >> ((3 - i%4)*8+4)) & 0xF) +
			hex_tab.charAt((binarray[i>>2] >> ((3 - i%4)*8  )) & 0xF);
		}
		return str;
	}

	s = Utf8Encode(s);
	return binb2hex(core_sha256(str2binb(s), s.length * chrsz));

}

//对于ajax进行再次封装，显示loading
function cpic_ajax(opts,show,hide){
	var url="../../../../"+opts.url;
	$.ajax({
		type:"post",
		url: url,
		data:JSON.stringify(opts.data),
		dataType: "json",
		cache: false,
		contentType: 'application/json;charset=utf-8',
		beforeSend:function(){
			if(show)
				showLoading();
		},
		complete:function(){
			if(!hide)
				hideLoading();
		},
		success:function(response){
			if(response.message.code=='success'){
				opts.success(response);
			}else{
				dialogs.error(response.message.message,"请求出错啦！");
			}
		},error: function(xmlHttpRequest,textStatus,errorThrow){
			if(xmlHttpRequest.readyState!=4){
				//dialogs.warning("操作过于频繁！",warningTitle);
			}else{
				var sessionStatus = xmlHttpRequest.getResponseHeader("sessionStatus");
				if(sessionStatus=="timeOut"){
					dialogs.error("SESSION超时啦！",errorTittle,['confirm'],function(){
						window.location.href='../../../../j_spring_security_logout';
					});
				}else{
					dialogs.error(strToJson(xmlHttpRequest.responseText).message.message,"请求出错啦！");
				}
			}
		}
	});

}

function hideLoading(){
	$("#loading").hide();
}

function showLoading(){
	$("#loading").show();
}


var dialogs={
	error:function(content,title,buttonArr,subfunction,cancelfunction){
		$("body").append(createDialogHtml(content,title,"error",buttonArr,subfunction,cancelfunction));
	},
	warning:function(content,title,buttonArr,subfunction,cancelfunction){
		$("body").append(createDialogHtml(content,title,"warning",buttonArr,subfunction,cancelfunction));
	},
	notify:function(content,title,buttonArr,subfunction,cancelfunction){
		$("body").append(createDialogHtml(content,title,"notify",buttonArr,subfunction,cancelfunction));
	}
};

function createDialogHtml(content,title,type,buttonArr,subfunction,cancelfunction){
	var loadDiv=$("<div>").addClass("loding_bj noticeDialog");
	loadDiv.append($("<div>").addClass("float-layer"));
	var contentDiv;
	if(type == "warning"){
		contentDiv=$("<div>").addClass("float-content").css({"height":"auto" , "top":"25%" , "width":"360px" ,"left":"50%" ,"marginLeft":"-180px"});
	}else{
		contentDiv=$("<div>").addClass("float-content").css({"height":"auto" , "top":"25%" , "width":"300px" ,"left":"40%" });
	}
	var color;
	if(type == "warning"){
		loadDiv.addClass("notice_big");
	}
	switch(type){
		case "error":color="#d2322d";break;
		case "warning":color="#f0ad4e";break;
		case "notify":color="#428bca";break;
	}
	var h=$("<h4>").css("background-color", color);
	var bindSpan=$("<span>").addClass("ng-binding").html(title);
	/*var closeButton=$("<button>").addClass("close").attr("type","button").html("X");
	 closeButton.click(function(){
	 closeDialog();
	 })*/
	h.append(bindSpan);//.append(closeButton);
	var textDiv;
	if(type == "warning"){
		textDiv=$("<div>").css({"overflow": "auto","min-height":"70px","max-height":"240px", "width":"94%","font-size":"13px","padding":"9px","padding-top":"15px"}).html(content);
	}else{
		textDiv=$("<div>").css({"overflow": "auto","min-height":"70px","max-height":"200px", "width":"94%","font-size":"14px","padding":"9px","padding-top":"15px"}).html(content);
	}
	var footDiv=$("<div>").css({"overflow": "hidden","margin-bottom":"20px"});
	var closeA=$("<a>").addClass("o-c-btn_a cursor").html("关闭");
	var subA=$("<a>").addClass("o-c-btn_a cursor").html("确认");
	var yesA=$("<a>").addClass("o-c-btn_a cursor").html("是");
	var noA=$("<a>").addClass("o-c-btn_a cursor").html("否");

	if(buttonArr){
		if(buttonArr[0]!=buttonArr[1]){
			addButton(buttonArr[1]);
			addButton(buttonArr[0]);
		}else{
			addButton(buttonArr[0]);
		}
	}else{
		footDiv.append(closeA);
	}

	noA.click(function(){
		if(cancelfunction)
			cancelfunction();
		else
			closeDialog();
	});

	closeA.click(function(){
		if(cancelfunction)
			cancelfunction();
		else
			closeDialog();
	});


	contentDiv.append(h).append(textDiv).append(footDiv);
	loadDiv.append(contentDiv);

	return loadDiv;

	function addButton(showButton){
		var button;
		switch(showButton){
			case "close":
				button=closeA;
				break;
			case "confirm":
				if(subfunction){
					subA.click(function(){
						subfunction();
					})
				}
				button=subA;
				break;
			case "yes":
				if(subfunction){
					yesA.click(function(){
						subfunction();
					})
				}
				button=yesA;
				break;
			case "no":
				button=noA;
				break;
		}
		footDiv.append(button);
	}


}
function closeDialog(){
	$(".noticeDialog").remove();
}
function passwordKey(id){
	$("#"+id).bind('keyup',function(){
		var v = $(this).val();
		$(".progress-bar-danger").css("width","");
		$(".progress-bar-warning").css("width","");
		$(".progress-bar-info").css("width","");
		$(".progress-bar-success").css("width","");
		if(v){
			var lv = isSecurity(v);
			if(lv==0){
				$(this).parent().find(".progress").show();
				$(this).parent().find(".progress-bar-danger").css("width","25%");
			}else if(lv==1){
				$(this).parent().find(".progress").show();
				$(this).parent().find(".progress-bar-danger").css("width","25%");
				$(this).parent().find(".progress-bar-warning").css("width","25%");
			}else if(lv==2){
				$(this).parent().find(".progress").show();
				$(this).parent().find(".progress-bar-danger").css("width","25%");
				$(this).parent().find(".progress-bar-warning").css("width","25%");
				$(this).parent().find(".progress-bar-info").css("width","25%");
			}else if(lv==3||lv==4){
				$(this).parent().find(".progress").show();
				$(this).parent().find(".progress-bar-danger").css("width","25%");
				$(this).parent().find(".progress-bar-warning").css("width","25%");
				$(this).parent().find(".progress-bar-info").css("width","25%");
				$(this).parent().find(".progress-bar-success").css("width","25%");
			}
		}else{
			$(this).parent().find(".progress").hide();
		}
	});
}


/**
 * @author c_liuzhigang
 */
$(function(){
	$.initContext({
		pageCode:"audit_info",
		ajaxStartCallback: showLoading,
		ajaxCompleteCallback: hideLoading,
		dataLoadedCallback:function(){
			setTime($.pageContext.userConfigs.currentDate);
		}
	});

	if(window.location.href.indexOf("#")!=-1){
		param = {
				"meta":{},
				"redata":{quotationNo:getAnchor()}
			   };
		
		cpic_ajax({
			url:"auditInformation/queryAuditInformation",
			data:param,
			success:function(response){
			if(response.result != null){
				createTable(response.result,$("#resultTable"),$(".tbClone"));
			}
		}
		},true);
	}
	
	
	
	$("#close").click(function(){
		window.close();
	});
	
	
	
});
	
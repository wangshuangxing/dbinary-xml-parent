$(function(){
    getHeader("");

    $.initContext({
        pageCode:"search",
        ajaxStartCallback: showLoading,
        ajaxCompleteCallback: hideLoading,
        dataLoadedCallback:function(){
            setTime($.pageContext.userConfigs.currentDate);
        }
    });
});
$(function() {
	$("#sendInfo").click(function() {
		if ($("#mobilePhone").val() == "") {
			$("#err").empty();
			$("#err").append("手机号码不能为空！");
			return;
		}
		if ($("#mobilePhone").val().length != 11) {
			$("#err").empty();
			$("#err").append("手机号码位数错误！");
			return;
		}
		if (!/^1[3|4|5|7|8]\d{9}$/.test($("#mobilePhone").val())) {
			$("#err").empty();
			$("#err").append("手机号码格式错误！");
			return;
		}

		settime();
		cpic_ajax({
			type : "POST",
			url : "send/sendSMS",
			data : {
				"meta" : {},
				"redata" : {
					"phone" : $('#mobilePhone').val()
				}
			},
			success : function(data) {
				// alert("发送成功！");
				var msg = data.message;
				if (msg) {
					if (msg.code == "success") {
						// dialogs.notify(msg.message,notifyTittle);
					}
					if (msg.code == "failed") {
						dialogs.warning("短信发送失败，请重新发送！", warningTitle);
					}
				}
			},
			error : function(data) {
				dialogs.warning("短信发送失败，请重新发送！", warningTitle);
			}
		});
	});

	$("#j_login")
			.click(
					function() {
						if (!/^1[3|4|5|7|8]\d{9}$/
								.test($("#mobilePhone").val())) {
							$("#err").empty();
							$("#err").append("请输入正确的手机号！");
							return false;
						}
						if ($("#verifyCode").val() == "") {
							$("#err").empty();
							$("#err").append("请输入验证码！");
							return false;
						}

						$("#j_login").attr("disabled", true).text("登录中...");

						$
								.ajax({
									type : "POST",
									dataType : "text",
									url : "../../../../j_spring_security_check",
									data : "mobile_phone="
											+ $('#mobilePhone').val()
											+ "&verify_code="
											+ $('#verifyCode').val(),

									success : function(data) {
										var da = $.parseJSON(data);
										if (da.authentication == "true") {
											if (da.reqUri) {
												window.top.location.href = da.reqUri;
												return;
											}
										} else {
											$("#err").empty();

											var errCode = da.errCode;
											var errMsg = "登录失败，请稍候再试！";

											if (errCode == "1") {
												errMsg = "手机号输入错误！";
											}
											if (errCode == "2") {
												errMsg = "用户已被锁定！";
											}
											if (errCode == "4") {
												errMsg = "验证码错误！";
											}

											$("#err").append(errMsg);
										}

									},
									error : function(xmlHttpRequest,
											textStatus, errorThrow) {
										if (xmlHttpRequest.readyState != 4) {
											// dialogs.warning("操作过于频繁！",warningTitle);
										} else {
											var sessionStatus = xmlHttpRequest
													.getResponseHeader("sessionStatus");
											if (sessionStatus == "timeOut") {
												dialogs
														.error(
																"SESSION超时啦！",
																errorTittle,
																[ 'confirm' ],
																function() {
																	window.location.href = '../../../../j_spring_security_logout';
																})
											} else {
												dialogs
														.error(
																JSON
																		.parse(xmlHttpRequest.responseText).message.message,
																"请求出错啦！")
											}
										}
									},

									complete : function(XMLHttpRequest) {
										if (XMLHttpRequest.readyState < 4) {
											ajaxReq.abort();
											dialogs.error("请求超时！", errorTittle);
										}
										$("#j_login").attr("disabled", false).text("登录");
									}
								});
					});

	var countdown = 60;
	function settime() {
		var obj = $("#sendInfo");
		if (countdown == 0) {
			obj.removeAttr("disabled");
			obj.val("发送验证码");
			countdown = 60;
			return;
		} else {
			obj.attr("disabled", true);
			obj.val("重新发送(" + countdown + ")秒");
			countdown--;
		}
		setTimeout(function() {
			settime()
		}, 1000)
	}

});

// 对于ajax进行再次封装，显示loading
function cpic_ajax(opts, show, hide) {
	var url = "../../../../" + opts.url;
	$.ajax({
		type : "post",
		url : url,
		data : JSON.stringify(opts.data),
		dataType : "json",
		cache : false,
		contentType : 'application/json;charset=utf-8',
		beforeSend : function() {
			if (show)
				showLoading();
		},
		complete : function() {
			if (!hide)
				hideLoading();
		},
		success : function(response) {
			if (response.message.code == 'success') {
				opts.success(response);
			} else {
				dialogs.error(response.message.message, "请求出错啦！")
			}
		},
		error : function(xmlHttpRequest, textStatus, errorThrow) {
			if (xmlHttpRequest.readyState != 4) {
				// dialogs.warning("操作过于频繁！",warningTitle);
			} else {
				var sessionStatus = xmlHttpRequest
					.getResponseHeader("sessionStatus");
				if (sessionStatus == "timeOut") {
					dialogs
						.error(
						"SESSION超时啦！",
						errorTittle,
						[ 'confirm' ],
						function() {
							window.location.href = '../../../../j_spring_security_logout';
						})
				} else {
					dialogs
						.error(
						strToJson(xmlHttpRequest.responseText).message.message,
						"请求出错啦！")
				}
			}
		}
	});

}

function hideLoading() {
	$("#loading").hide();
}

function showLoading() {
	$("#loading").show();
}

var dialogs = {
	error : function(content, title, buttonArr, subfunction, cancelfunction) {
		$("body").append(
				createDialogHtml(content, title, "error", buttonArr,
						subfunction, cancelfunction));
	},
	warning : function(content, title, buttonArr, subfunction, cancelfunction) {
		$("body").append(
				createDialogHtml(content, title, "warning", buttonArr,
						subfunction, cancelfunction));
	},
	notify : function(content, title, buttonArr, subfunction, cancelfunction) {
		$("body").append(
				createDialogHtml(content, title, "notify", buttonArr,
						subfunction, cancelfunction));
	}
}

function createDialogHtml(content, title, type, buttonArr, subfunction,
		cancelfunction) {
	var loadDiv = $("<div>").addClass("loding_bj noticeDialog");
	loadDiv.append($("<div>").addClass("float-layer"));
	var contentDiv;
	if(type == "warning"){
		contentDiv=$("<div>").addClass("float-content").css({"height":"auto" , "top":"25%" , "width":"360px" ,"left":"50%" ,"marginLeft":"-180px"});
	}else{
		contentDiv=$("<div>").addClass("float-content").css({"height":"auto" , "top":"25%" , "width":"300px" ,"left":"40%" });
	}
	var color;
	switch (type) {
	case "error":
		color = "#d2322d";
		break;
	case "warning":
		color = "#f0ad4e";
		break;
	case "notify":
		color = "#428bca";
		break;
	}
	var h = $("<h4>").css("background-color", color);
	var bindSpan = $("<span>").addClass("ng-binding").html(title);
	/*
	 * var closeButton=$("<button>").addClass("close").attr("type","button").html("X");
	 * closeButton.click(function(){ closeDialog(); })
	 */
	h.append(bindSpan);// .append(closeButton);
	var textDiv;
	if(type == "warning"){
		textDiv=$("<div>").css({"overflow": "auto","min-height":"70px","max-height":"240px", "width":"94%","font-size":"13px","padding":"9px","padding-top":"15px"}).html(content);
	}else{
		textDiv=$("<div>").css({"overflow": "auto","min-height":"70px","max-height":"200px", "width":"94%","font-size":"14px","padding":"9px","padding-top":"15px"}).html(content);
	}
	var footDiv = $("<div>").css({
		"overflow" : "hidden",
		"margin-bottom" : "20px"
	});
	var closeA = $("<a>").addClass("o-c-btn_a cursor").html("关闭");
	var subA = $("<a>").addClass("o-c-btn_a cursor").html("确认");
	var yesA = $("<a>").addClass("o-c-btn_a cursor").html("是");
	var noA = $("<a>").addClass("o-c-btn_a cursor").html("否");

	if (buttonArr) {
		if (buttonArr[0] != buttonArr[1]) {
			addButton(buttonArr[1]);
			addButton(buttonArr[0]);
		} else {
			addButton(buttonArr[0]);
		}
	} else {
		footDiv.append(closeA);
	}

	noA.click(function() {
		if (cancelfunction)
			cancelfunction();
		else
			closeDialog();
	})

	closeA.click(function() {
		if (cancelfunction)
			cancelfunction();
		else
			closeDialog();
	})

	contentDiv.append(h).append(textDiv).append(footDiv);
	loadDiv.append(contentDiv);

	return loadDiv;

	function addButton(showButton) {
		var button;
		switch (showButton) {
		case "close":
			button = closeA;
			break;
		case "confirm":
			if (subfunction) {
				subA.click(function() {
					subfunction();
				})
			}
			button = subA;
			break;
		case "yes":
			if (subfunction) {
				yesA.click(function() {
					subfunction();
				})
			}
			button = yesA;
			break;
		case "no":
			button = noA;
			break;
		}
		footDiv.append(button);
	}

}
function closeDialog() {
	$(".noticeDialog").remove();
}

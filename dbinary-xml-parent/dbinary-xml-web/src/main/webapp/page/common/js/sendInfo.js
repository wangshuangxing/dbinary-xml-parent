/**
 * Created by liuxianyi on 2017-06-05.
 */
var oTime,
    flag;
$(function(){

    $.initContext({
        pageCode:"send_info",
        ajaxStartCallback: showLoading,
        ajaxCompleteCallback: hideLoading,
        dataLoadedCallback:function(data){
            initPageData($.pageContext);
        }
    });

    if(window.location.href.indexOf("#") != -1){
        var params = {
            "meta":{},
            "redata":{quotationNo:getAnchor()}
        };

        cpic_ajax({
            data:params,
            url:"quotationPolicy/queryTaskDetail",
            success:function(response){

                $.each(response.result,function(key,value){
                    if(key == "emsWay" && value){
                        if(value==1){
                            $("#platInfo").find("[data="+key+"]").text("自取");
                        }else if(value==2){
                            $("#platInfo").find("[data="+key+"]").text("寄送");
                        }else{
                            $("#platInfo").find("[data="+key+"]").text(value || "");
                        }
                    }else if(key == "status"){
                        $("#platInfo").find("[data="+key+"]").text("");
                    }else{
                        $("#platInfo").find("[data="+key+"]").text(value || "");
                    }
                });

            }
        },true);

    }

    $("[name='takeTime']").focus(function(){
        var oDate = new Date();
        WdatePicker({
            "minDate":today+" "+oDate.getHours()+":"+oDate.getMinutes()+":"+oDate.getSeconds(),
            "startDate":today+" "+oDate.getHours()+":"+oDate.getMinutes()+":"+oDate.getSeconds(),
            "dateFmt":"yyyy-MM-dd HH:mm"
        });

        flag = false;
    });

    $("#sendInfoForm").find("input,select").focus(function(){
        if($(this).attr("name") != "takeTime" && $(".datePlaugin").length && $(".datePlaugin").css("display") == "block"){
            $(".datePlaugin").hide();
            checkdate();
        }
    });

    //时间范围校验
    function checkdate(){
        if($("[name='takeTime']").val()){
            var oTimes = $("[name='takeTime']").val().split(" ");
            if(oTime){
                if(oTime.endworktime || oTime.begworktime){
                    if((oTime.endworktime && parseInt(oTimes[1].replace(/:/g,"")) > parseInt(oTime.endworktime.replace(/:/g,""))) || (oTime.begworktime && parseInt(oTimes[1].replace(/:/g,"")) < parseInt(oTime.begworktime.replace(/:/g,"")))){
                        dialogs.warning("当前所选时间不在有效范围内"+oTime.begworktime+"-"+oTime.endworktime,notifyTittle);
                        $("[name='takeTime']").val("");
                        return false;
                    }
                }

                if(oTime.notworktime.length){
                    for(var i=0;i<oTime.notworktime.length;i++){
                        if(parseInt(oTimes[0].replace(/-/g,"")) == parseInt(oTime.notworktime[i].replace(/-/g,""))){
                            dialogs.warning("非工作日不能自取，请核实！ ",notifyTittle);
                            $("[name='takeTime']").val("");
                            return false;
                        }
                    }
                }
            }
        }
    }


    $("body").click(function(){
        flag = true;
        if($(".datePlaugin").length){
            $(".datePlaugin").hide();
        }
        checkdate();
    });

    $("[name='takeTime']").blur(function(){
        if($(".datePlaugin").length && $(".datePlaugin").css("display") == "block"){
            return false;
        }
        checkdate();
    });

    //配送发起信息弹框显示
    $("#sure").click(function(){
        cpic_ajax({
            data:{
                "meta":{},
                "redata":{}
            },
            url:"quotationPolicy/PickUp",
            success:function(response){
                $("#sendInfoDialog").show();
                var oPtions = response.result.envelopeInfo || "",
                    i=0,
                    len = oPtions.length;
                if(len){
                    for(;i<len;i++){
                        oPtions[i].name = oPtions[i].envelope;
                        oPtions[i].code = oPtions[i].imgCode;
                    }
                    createOption($("[name='envelopeSize']"), oPtions);
                }
                var aDdress = response.result.addressInfos || "",
                    i=0,
                    aLen = aDdress.length;
                if(aLen){
                    for(;i<aLen;i++){
                        aDdress[i].name = aDdress[i].address;
                    }
                    createOptions($("[name='addressCode']"), aDdress);
                }
                oTime = response.result.timeInfo || "";
            }
        },true);
    });

    $("[name='addressCode']").change(function(){
        $("[name='takeAddress']").val($("[name='addressCode'] option:checked").attr("data") || "");
    });

    function createOptions(element,arr,defultVal,type){
        if(!arr){
            return false;
        }
        if(type){
            element.html("<option value=''></option>");
        }else{
            element.html("<option value=''>请选择</option>");
        }

        for(var i=0;i<arr.length;i++){
            var select="";
            if(defultVal){
                if(arr[i].code==defultVal)
                    select="selected='selected'";
            }

            var option="<option value='"+arr[i].code+"' data='"+arr[i].storeAddress+"' "+select+">"+arr[i].name+"</option>";
            element.append(option);
        }
    }

    //配送发起信息弹框隐藏
    $("#sendInfoDialog").find(".preview").click(function(){
        $("#sendInfoDialog").hide();
        var i = 0,
            len = $("#sendInfoForm").find("select,input").length;
        for(;i<len;i++){
            $("#sendInfoForm").find("select,input").eq(i).val("");
        }
        $(".check-type").prop("checked",false);
        $(".check-type").eq(0).prop("checked",true);
    });

    //配送发起信息确定
    $("#sendInfoDialog").find(".confirm").click(function(){
        if($("#selfGet").prop("checked")){//自取
            if(!$("#sendInfoDialog [name='takeTime']").val()){
                dialogs.warning("请选择取件时间",notifyTittle);
                return false;
            }
            if(!$("#sendInfoDialog [name='addressCode']").val()){
                dialogs.warning("请选择网点名称",notifyTittle);
                return false;
            }
        }else if($("#postSend").prop("checked")){//寄送
            if(!$("#sendInfoDialog [name='custName']").val()){
                dialogs.warning("请输入收件人姓名",notifyTittle);
                return false;
            }
            if(!$("#sendInfoDialog [name='custAddress']").val()){
                dialogs.warning("请输入收件人地址",notifyTittle);
                return false;
            }
            if(!$("#sendInfoDialog [name='custMobile']").val()){
                dialogs.warning("请输入收件人手机号",notifyTittle);
                return false;
            }
            if((!reg.mobile($.trim($("#sendInfoDialog [name='custMobile']").val())) && $.trim($("#sendInfoDialog [name='custMobile']").val()) != "")) {
                dialogs.warning("请输入正确的手机号码!", notifyTittle);
                return false;
            }
            if(!$("#sendInfoDialog [name='envelopeSize']").val()){
                dialogs.warning("请选择信封尺寸",notifyTittle);
                return false;
            }
        }else{
            dialogs.warning("请选择配送方式",notifyTittle);
            return false;
        }
        cpic_ajax({
            data:{
                "meta":{},
                "redata":{
                    "quotationNo":getAnchor(),
                    "emsway":$("#selfGet").prop("checked")?1:2,
                    "takeTime":$("#selfGet").prop("checked")?$("#sendInfoDialog [name='takeTime']").val():"",
                    "addressCode":$("#selfGet").prop("checked")?$("#sendInfoDialog [name='addressCode']").val():"",
                    "takeAddress":$("#selfGet").prop("checked")?$("#sendInfoDialog [name='addressCode'] option:checked").html():"",
                    "custName":$("#selfGet").prop("checked")?"":$("#sendInfoDialog [name='custName']").val(),
                    "custAddress":$("#selfGet").prop("checked")?"":$("#sendInfoDialog [name='custAddress']").val(),
                    "custMobile":$("#selfGet").prop("checked")?"":$("#sendInfoDialog [name='custMobile']").val(),
                    "envelopeCode":$("#selfGet").prop("checked")?"":$("#sendInfoDialog [name='envelopeSize']").val(),
                    "envelopeSize":$("#selfGet").prop("checked")?"":$("#sendInfoDialog [name='envelopeSize'] option:checked").html()
                }
            },
            url:"quotationPolicy/DeliveryStart",
            success:function(data){
                dialogs.notify(data.message.message, notifyTittle,["yes"],function(){
                    closeDialog();
                    $("#sendInfoDialog").find(".preview").click();
                });
            }
        },true);
    });

    //配送信息自取、寄送单选
    $(".check-type").click(function(){
        $(".check-type").prop("checked",false);
        $(this).prop("checked",true);
    });

    //页面关闭
    $("#close").click(function(){
        window.close();
    });

});
$(function(){
	var dataRequired = {
        oldPwd:{name:"原始密码"},
        newPwd:{name:"新密码"},
        confirmNewPwd:{name:"确认新密码"}
    };

	$.initContext({
		pageCode:"update_password",
		ajaxStartCallback: showLoading,
		ajaxCompleteCallback: hideLoading,
		dataLoadedCallback:function(){
			setTime($.pageContext.userConfigs.currentDate);
		}
	});

    passwordKey('newPwd');

	$("#confirm").click(function(){
		var oldPwd = $("#oldPwd").val();
		var newPwd = $("#newPwd").val();
		var confirmNewPwd = $("#confirmNewPwd").val();
		
		var flag = true;

		$(".required").each(function(){
            if($(this).val()==""){
                flag=false;
                dialogs.warning(dataRequired[$(this).attr("name")].name+" 未填写!","提示");
                return false;
            }
        });

		if(!flag){
			return false;
		}

		if (newPwd.length > 30) {
			dialogs.warning("用户名、密码字符长度不能超过30！", "提示");
			return false;
		}

		if (newPwd.length > 30) {
			dialogs.warning("新密码字符长度不能超过30！", "提示");
			return false;
		}

		var isSecurityFlg = isSecurity(newPwd);
		if(isSecurityFlg != 4 && isSecurityFlg != 3){
			dialogs.warning("新密码不符合安全要求！<密码长度需要大于8位，包含数字、小写字母、大写字母或特殊字符>", "提示");
			return false;
		}

		if(newPwd != confirmNewPwd){
			dialogs.warning("新密码与确认新密码不匹配","提示");
		}else if(oldPwd == newPwd){
			dialogs.warning("新密码与旧密码不能相同","提示");
		}else{
			param = {"meta":{},"redata":{oldPwd:oldPwd,newPwd:newPwd}};
			cpic_ajax({
				url:"user/changePassword",
				data:param,
				success:function(response){
					dialogs.notify("修改成功!",notifyTittle,["confirm"],function(){
						window.location.href="../../../../j_spring_security_logout";
					});
				}
			},true);
		}
	});

	$("#canel").click(function(){

		window.history.back();
				
	});

});

function passwordKey(id){
	$("#"+id).bind('keyup',function(){
		var v = $(this).val();
		$(".progress-bar-danger").css("width","");
		$(".progress-bar-warning").css("width","");
		$(".progress-bar-info").css("width","");
		$(".progress-bar-success").css("width","");
		if(v){
			var lv = isSecurity(v);
			if(lv==0){
				$(this).parent().find(".progress").show();
				$(this).parent().find(".progress-bar-danger").css("width","25%");
			}else if(lv==1){
				$(this).parent().find(".progress").show();
				$(this).parent().find(".progress-bar-danger").css("width","25%");
				$(this).parent().find(".progress-bar-warning").css("width","25%");
			}else if(lv==2){
				$(this).parent().find(".progress").show();
				$(this).parent().find(".progress-bar-danger").css("width","25%");
				$(this).parent().find(".progress-bar-warning").css("width","25%");
				$(this).parent().find(".progress-bar-info").css("width","25%");
			}else if(lv==3||lv==4){
				$(this).parent().find(".progress").show();
				$(this).parent().find(".progress-bar-danger").css("width","25%");
				$(this).parent().find(".progress-bar-warning").css("width","25%");
				$(this).parent().find(".progress-bar-info").css("width","25%");
				$(this).parent().find(".progress-bar-success").css("width","25%");
			}
		}else{
			$(this).parent().find(".progress").hide();
		}
	});
}



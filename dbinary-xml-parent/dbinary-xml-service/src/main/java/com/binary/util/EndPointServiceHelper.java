/**
 * 
 */
package com.binary.util;

import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.binary.xml.Document;
import com.binary.xml.ObjectFactory;

/**
 * @author dbinary
 * @date 2018年8月4日 下午10:33:17
 */
public class EndPointServiceHelper {
	private static JAXBContext jc;
	private static final Logger logger = LoggerFactory.getLogger(EndPointServiceHelper.class);
	static{
		try {
			 jc = JAXBContext.newInstance("com.binary.xml");
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}

	/**
	 * xml to oject
	 */
	public static Document xml2Obj(String retMsg) throws Exception{
	
		Unmarshaller u = getUnmarshaller();
		Document document = new Document();

		JAXBElement<?> poe = (JAXBElement<?>) u.unmarshal(new StreamSource(
				new StringReader(retMsg)));

		document = (Document) poe.getValue();
		return document;
	}
	/**
	 * obj ---xml
	 */
	public static String obj2Xml(Document document) throws Exception{
		String result ="";
		JAXBElement<Document> element = (new ObjectFactory())
				.createDocument(document);
		try {
			Marshaller ms =  getMarshaller();
			ByteArrayOutputStream byteArr = new ByteArrayOutputStream();
			OutputStreamWriter write = new OutputStreamWriter(byteArr);
			
			ms.marshal(element, write);
			result = new String(byteArr.toByteArray());
		} catch (JAXBException e) {
			logger.info("JAXB object2xml Exception : " + e.getMessage());
		}
		return result;
	}

	
	private static Unmarshaller getUnmarshaller(){
		jc = getJAXBContext();
		Unmarshaller ms= null;
			try {
				 ms = jc.createUnmarshaller();
			} catch (JAXBException e) {
				e.printStackTrace();
			}
		return ms;
	}
//	得到 objct TO xml OPJO
	private static Marshaller getMarshaller(){
		jc = getJAXBContext();
		Marshaller m = null;
		try {
			m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.setProperty(Marshaller.JAXB_ENCODING, "GBK");
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return m;
	}
	/**
	 *得到JAXBCONTEXT
	 */
	private static JAXBContext getJAXBContext(){
		if(jc==null){
			try {
				jc = JAXBContext.newInstance("com.binary.xml");
			} catch (JAXBException e) {
				e.printStackTrace();
			}
		}
		return jc;
	}
	public static void main(String[] args) throws Exception {
		//---------------
		StringBuilder retMsg = new StringBuilder()
				.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>")
				.append("<document>")
				.append("<response>")
				.append("<body>")
				.append("<resultCode>0000</resultCode>")
				.append("<resultMsg>成功</resultMsg>")
				.append("<userInfo>"
						+ "<name>张慧慧</name>"
						+ "<code>userInfo</code>"
						+ "<gender>女</gender>"
					+ "</userInfo>")
				.append("</body>")
				.append("</response>")
				.append("</document>");
		//-----------------------
		Document document = xml2Obj(retMsg.toString());
		System.out.println(document.getResponse().getBody());
		/*Document document = new Document();
		Request request = new Request();
		Body body = new Body();
		body.setResultCode("code");
		body.setResultMsg("成功了大辉辉");
		UserInfo userInfo = new UserInfo();
		userInfo.setCode("userInfo");
		userInfo.setName("张慧慧");
		userInfo.setGender("女");
		body.setUserInfo(userInfo);
	    request.setBody(body);
		 document.setRequest(request);
		 String xml = obj2Xml(document);
		System.out.println(xml);*/
	}

}

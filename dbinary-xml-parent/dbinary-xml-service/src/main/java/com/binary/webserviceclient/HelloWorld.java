/**
 * HelloWorld.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.binary.webserviceclient;

public interface HelloWorld extends java.rmi.Remote {
    public java.lang.String getInfo(java.lang.String info) throws java.rmi.RemoteException;
}

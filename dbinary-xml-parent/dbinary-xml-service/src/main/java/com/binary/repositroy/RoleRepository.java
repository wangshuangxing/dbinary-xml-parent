package com.binary.repositroy;



import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.binary.entity.Role;
import com.binary.repositroy.custom.RoleRepositoryCustom;

public interface RoleRepository extends EntityRepository<Role, Integer> , RoleRepositoryCustom {

	@Query("select ue from Role ue where name =:name")
	public List<Role> findByRoleName(@Param("name") String name);
}

package com.binary.repositroy.custom;

import java.util.List;



import com.binary.entity.Role;

public interface RoleRepositoryCustom {
	//findByabcName
	List<Role> findListByName(String name);
	public Role findOneByName(String name);

}

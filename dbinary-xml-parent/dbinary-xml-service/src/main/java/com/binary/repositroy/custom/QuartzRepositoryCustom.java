/**
 * 
 */
package com.binary.repositroy.custom;

import java.util.List;

import com.binary.entity.Quartz;

/**
 * @author dbinary
 * @date 2018年7月21日 下午11:18:13
 */
public interface QuartzRepositoryCustom {
	
	List<Quartz> getAllQuartz();

}

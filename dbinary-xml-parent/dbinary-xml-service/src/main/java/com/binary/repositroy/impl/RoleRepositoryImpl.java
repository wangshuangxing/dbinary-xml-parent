package com.binary.repositroy.impl;


import java.util.ArrayList;
import java.util.List;

/*import org.springframework.data.domain.Page;

import com.binary.commons.Pager;*/
import com.binary.entity.Role;
import com.binary.repositroy.custom.RoleRepositoryCustom;
import com.binary.repositroy.hibernate.HibernateBaseFace;

public class RoleRepositoryImpl extends HibernateBaseFace<Role> implements RoleRepositoryCustom {

	public List<Role> findListByName(String name) {
		List<Object> list = new ArrayList<Object>();
		StringBuilder sb = new StringBuilder();
		sb.append("select * from c_role where 1=1 ");
		if(name!=null&&!name.equals("")){
			sb.append(" and role_name = ?");
			list.add(name);
		}
		List<Role> elements = this.findAllEntityByObjectArrSql(sb.toString(),list.toArray() );
		List<Role> result =null;
		if(elements!=null&&elements.size()>0){
			int size = elements.size();
			result = new ArrayList<Role>();
			for(int i=0;i<size;i++){
				Role obj = elements.get(i);
				result.add(obj);
			
			}
		}	
		return result;
	}

	public Role findOneByName(String name) {
		List<Object> list = new ArrayList<Object>();
		StringBuilder sb = new StringBuilder();
		sb.append("select * from c_role where 1=1 ");
		sb.append("and role_name=?");
		list.add(name);
		Role role = this.findOneEntityByObjectArrSql(sb.toString(), list.toArray());
		return role;
	}
	

	
}

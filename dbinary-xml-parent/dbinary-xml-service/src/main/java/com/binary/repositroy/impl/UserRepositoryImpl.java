package com.binary.repositroy.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Page;

import com.binary.commons.Pager;
import com.binary.entity.User;
import com.binary.repositroy.custom.UserRepositoryCustom;
import com.binary.repositroy.hibernate.HibernateBaseFace;

public class UserRepositoryImpl  extends HibernateBaseFace<User>  implements UserRepositoryCustom {

	@Override
	public Page<Object[]> findByUserName(String name){
		List<Object> list = new ArrayList<Object>();
		StringBuilder sb = new StringBuilder();
		sb.append("select * from c_user where 1=1 ");
		if(name!=null&&!name.equals("")){
			sb.append(" and name = ?");
			list.add(name);
		}
		Pager pageable = new Pager();
		pageable.setPageNo(1);
		pageable.setPageSize(5);
		Page<Object[]> page = this.getResultsByObjectArrSql(sb.toString(),list.toArray(),pageable);
		return page;
	}

	@Override
	public User findEntityByName(String name,String address) {
		List<Object> list = new ArrayList<Object>();
		StringBuilder sb = new StringBuilder();
		sb.append("select id,name,age,sex,address,job from c_user where 1=1 ");
		if(!"".equals(name)&&null!=name){
			sb.append(" and name = ?");
			list.add(name);
		}
		if(!"".equals(address)&&address!=null){
			sb.append(" and address = ?");
			list.add(address);
		}
		return this.findOneEntityByObjectArrSql(sb.toString(), list.toArray());
	}

	@Override
	public List<User> findAllEntityByName(String name, String address) {
		List<Object> list = new ArrayList<Object>();
		StringBuilder sb = new StringBuilder();
		sb.append("select id,name,age,sex,address,salary,job from c_user where 1=1 ");
		if("".equals(name)&&null!=name){
			sb.append(" and name = ?");
			list.add(name);
		}
		if("".equals("address")&&address!=null){
			sb.append(" and address = ?");
			list.add(address);
		}
		return this.findAllEntityByObjectArrSql(sb.toString(), list.toArray());
	}
}

package com.binary.repositroy;





import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.binary.entity.User;
import com.binary.repositroy.custom.UserRepositoryCustom;
public interface UserRepository extends EntityRepository<User, Integer> , UserRepositoryCustom{
	@Query("select ue from User ue where name =:name")
	public User findByName(@Param("name") String name);
	
	@Modifying
	@Query("update User ue set ue.address =:address where ue.name=:name")
	public int updateByName(@Param("address")String address,@Param("name")String name);
}

/**
 * 
 */
package com.binary.repositroy;

import com.binary.entity.Quartz;
import com.binary.repositroy.custom.QuartzRepositoryCustom;

/**
 * @author dbinary
 * @date 2018年7月21日 下午11:17:10
 */
public interface QuartzRepository extends EntityRepository<Quartz, Integer> , QuartzRepositoryCustom {

}

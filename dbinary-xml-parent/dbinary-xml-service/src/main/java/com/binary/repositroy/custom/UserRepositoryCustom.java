package com.binary.repositroy.custom;

import java.util.List;

import org.springframework.data.domain.Page;

import com.binary.entity.User;

public interface UserRepositoryCustom {
 Page<Object[]> findByUserName(String name);
 User findEntityByName(String name,String address);
 List<User> findAllEntityByName(String name,String address);
}

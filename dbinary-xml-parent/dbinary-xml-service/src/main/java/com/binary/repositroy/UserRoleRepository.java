/**
 * 
 */
package com.binary.repositroy;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.binary.entity.UserRole;

/**
 * @author c_danyanxin
 * @date 2018年7月23日
 */
public interface UserRoleRepository extends EntityRepository<UserRole, Long> {

	@Query("select ur from UserRole ur where ur.userId=:userId ")
	public List<UserRole> findByUserId(@Param("userId") Long userId);
}

/**
 * 
 */
package com.binary.xml;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author dbinary
 * @date 2018年8月4日 下午10:18:27
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="request",propOrder={"head","body"})
public class Request implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 4746870920899637353L;
	@XmlElement(required = true)
    protected Head head;
    @XmlElement(required = true)
    protected Body body;
	public Head getHead() {
		return head;
	}
	public void setHead(Head head) {
		this.head = head;
	}
	public Body getBody() {
		return body;
	}
	public void setBody(Body body) {
		this.body = body;
	}
    

}

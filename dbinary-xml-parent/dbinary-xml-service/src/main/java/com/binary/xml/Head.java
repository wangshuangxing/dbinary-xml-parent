/**
 * 
 */
package com.binary.xml;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author dbinary
 * @date 2018年8月4日 下午10:22:01
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "head", propOrder = {
    "function",
    "functiontype",
    "channel",
    "reqMsgId"
   
})
public class Head implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = -1687749626814961481L;
	@XmlElement(required = true)
    protected String function;
    @XmlElement(required = true)
    protected String functiontype;
    @XmlElement(required = true)
    protected String channel;
    @XmlElement(required = true)
    protected String reqMsgId;
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	public String getFunctiontype() {
		return functiontype;
	}
	public void setFunctiontype(String functiontype) {
		this.functiontype = functiontype;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public String getReqMsgId() {
		return reqMsgId;
	}
	public void setReqMsgId(String reqMsgId) {
		this.reqMsgId = reqMsgId;
	}
}

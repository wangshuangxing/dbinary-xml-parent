/**
 * 
 */
package com.binary.xml;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author dbinary
 * @date 2018年8月4日 下午10:23:34
 * 报文体
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="body",propOrder={"userInfo","resultCode","resultMsg"})
public class Body implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2897214451233213370L;
	@XmlElement(required=true)
	protected String resultCode;
	@XmlElement(required=true)
	protected String resultMsg;
	@XmlElement(required=true)
	protected UserInfo userInfo;
	

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getResultMsg() {
		return resultMsg;
	}

	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	@Override
	public String toString() {
		return "Body [resultCode=" + resultCode + ", resultMsg=" + resultMsg + ", userInfo=" + userInfo + "]";
	}
	
	
}

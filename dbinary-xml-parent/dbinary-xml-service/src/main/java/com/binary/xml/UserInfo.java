/**
 * 
 */
package com.binary.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author dbinary
 * @date 2018年8月4日 下午10:25:14
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="userInfo",propOrder={"name","code","gender"})
public class UserInfo {
	@XmlElement(required=true)
	protected String name;
	@XmlElement(required=true)
	protected String code;
	@XmlElement(required=true)
	protected String gender;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	

}

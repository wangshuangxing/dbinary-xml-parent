/**
 * 
 */
package com.binary.xml;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author dbinary
 * @date 2018年8月4日 下午10:13:55
 * 对发送报文的封装
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="document",propOrder={"signature","request","response"})
public class Document implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 5228771571016267729L;
	@XmlElement(required = true)
    protected String signature;
    @XmlElement(required = true)
    protected Request request;
    @XmlElement(required = true)
    protected Response response;
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	public Request getRequest() {
		return request;
	}
	public void setRequest(Request request) {
		this.request = request;
	}
	public Response getResponse() {
		return response;
	}
	public void setResponse(Response response) {
		this.response = response;
	}
    
}

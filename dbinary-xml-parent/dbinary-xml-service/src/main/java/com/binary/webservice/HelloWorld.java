package com.binary.webservice;

import javax.jws.WebParam;
import javax.jws.WebService;
@WebService
/*@SOAPBinding(style = Style.RPC)*/
public interface HelloWorld {

	public String getInfo(@WebParam(name="info")String info);
}

package com.binary.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.binary.entity.User;
import com.binary.repositroy.UserRepository;
import com.binary.service.UserService;
@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserRepository userR;
	public User getUserInfo() {
		User list = userR.findOne(1);
		return list;
	}
	@Override
	public int updateUserByName(String address, String name) {
		return userR.updateByName(address, name);
	}
	@Override
	public User save(User user) {
		return userR.save(user);
	}

}

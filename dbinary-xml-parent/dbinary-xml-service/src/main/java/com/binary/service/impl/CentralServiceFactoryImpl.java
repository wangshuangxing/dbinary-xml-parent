/**
 * 
 */
package com.binary.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.binary.constant.ConstantFactory;
import com.binary.entity.FacadeFactory;
import com.binary.service.CentralServiceFactory;
import com.binary.util.EndPointServiceHelper;
import com.binary.util.HttpClientSendHelper;
import com.binary.xml.Body;
import com.binary.xml.Document;
import com.binary.xml.Request;
import com.binary.xml.UserInfo;

/**
 * @author c_danyanxin
 * @date 2018年8月10日
 */
@Service
public class CentralServiceFactoryImpl implements CentralServiceFactory {
	private final static Logger logger = LoggerFactory.getLogger(CentralServiceFactoryImpl.class);

	@Override
	public FacadeFactory<Body> getUserInfo(UserInfo userInfo) {
		
		//chichu data(url) from db
		String url ="http://192.168.174.1:8883/myProject/sendMsgXml.do";
		Body body = new Body();
		body.setUserInfo(userInfo);
		Request request = new Request();
		request.setBody(body);
		Document document = new Document();
		document.setRequest(request);
		FacadeFactory<Body> result = new FacadeFactory<Body>();
		return getResponse(url, request, document, result);
	}

	/**
	 * @param url
	 * @param request
	 * @param document
	 * @param result
	 */
	private FacadeFactory<Body> getResponse(String url, Request request, Document document,
			FacadeFactory<Body> result) {
		try {
			String requestXml = EndPointServiceHelper.obj2Xml(document);
			logger.info("/n"+"send RequestXMl:"+requestXml);
			String responseXml = HttpClientSendHelper.postXmlOrJson(url, requestXml, false);
			logger.info("/n"+"recevice ResponseXMl:"+responseXml);
			if(responseXml!=null&&!request.equals("")){
				HttpClientSendHelper.convert(HttpClientSendHelper.getResponseBodyFromXml(responseXml),result);
			}else{
				result.setErrorCode(ConstantFactory.ERROR);
				result.setErrorMsg("系统繁忙，请稍后重试");
			}
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
		}
		return result;
	}

}

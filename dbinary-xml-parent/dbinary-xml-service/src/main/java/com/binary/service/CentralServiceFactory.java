/**
 * 
 */
package com.binary.service;

import com.binary.entity.FacadeFactory;
import com.binary.xml.Body;
import com.binary.xml.UserInfo;

/**
 * xml or json 调用
 * @author dbinary
 * @date 2018年8月10日
 */
public interface CentralServiceFactory {
	
	FacadeFactory<Body> getUserInfo(UserInfo userInfo);
	
}

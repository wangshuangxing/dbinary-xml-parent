/**
 * 
 */
package com.binary.service;

import java.util.List;

import com.binary.entity.Quartz;

/**
 * @author dbinary
 * @date 2018年7月21日 下午11:28:45
 */
public interface QuartzService {
	List<Quartz> getAllQuartz();
}

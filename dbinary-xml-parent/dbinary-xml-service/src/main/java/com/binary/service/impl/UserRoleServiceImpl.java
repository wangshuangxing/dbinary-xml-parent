/**
 * 
 */
package com.binary.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.binary.entity.User;
import com.binary.entity.UserRole;
import com.binary.repositroy.UserRepository;
import com.binary.repositroy.UserRoleRepository;
import com.binary.service.UserRoleService;

/**
 * @author dbinary
 * @date 2018年7月23日 下午11:18:22
 */
@Service
public class UserRoleServiceImpl implements UserRoleService {
	@Autowired
	private UserRoleRepository userRoleRepository;
	@Autowired UserRepository userRepository;
	@Override
	public List<UserRole> getUserRoleInfo(String userName) {
		User user = userRepository.findByName(userName);
		 List<UserRole> list = userRoleRepository.findByUserId(user.getId());
		return list.isEmpty()?null:list;
	}

}

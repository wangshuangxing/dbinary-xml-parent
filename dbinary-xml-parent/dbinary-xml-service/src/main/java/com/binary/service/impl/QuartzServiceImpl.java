/**
 * 
 */
package com.binary.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.binary.entity.Quartz;
import com.binary.repositroy.QuartzRepository;
import com.binary.service.QuartzService;

/**
 * @author dbinary
 * @date 2018年7月21日 下午11:29:13
 */
@Service
public class QuartzServiceImpl implements QuartzService {

	@Autowired
	private QuartzRepository quartzRepository;
	@Override
	public List<Quartz> getAllQuartz() {
		Iterable<Quartz> iters = quartzRepository.findAll();
		List<Quartz> list = new ArrayList<Quartz>();
		for(Iterator<Quartz> it= iters.iterator();it.hasNext();){
			list.add(it.next());
		}
		return list;	
	}

}

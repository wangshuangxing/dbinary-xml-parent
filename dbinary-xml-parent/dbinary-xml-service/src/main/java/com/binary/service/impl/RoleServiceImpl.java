package com.binary.service.impl;

/*import org.slf4j.Logger;
import org.slf4j.LoggerFactory;*/
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.binary.entity.Role;
import com.binary.repositroy.RoleRepository;
import com.binary.service.RoleService;
@Service
public class RoleServiceImpl implements RoleService {
	@Autowired
	private RoleRepository roleR;
	public Role getRoleInfo(String name) {
		return roleR.findOneByName(name);
	}
	
	public List<Role> getListRoleInfo(String name){
		return roleR.findListByName(name);
	}

}

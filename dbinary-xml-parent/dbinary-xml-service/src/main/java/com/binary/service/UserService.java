package com.binary.service;


import com.binary.entity.User;

public  interface UserService {
	User getUserInfo();
	int updateUserByName(String address,String name);
	User save(User user);
}

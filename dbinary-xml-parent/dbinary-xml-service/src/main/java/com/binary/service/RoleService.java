package com.binary.service;


import java.util.List;

import com.binary.entity.Role;

public  interface RoleService {
	Role getRoleInfo(String name);
    List<Role> getListRoleInfo(String name);
}

/**
 * 
 */
package com.binary.service;

import java.util.List;

import com.binary.entity.UserRole;

/**
 * @author dbinary
 * @date 2018年7月23日 下午11:17:07
 */
public interface UserRoleService {

	List<UserRole> getUserRoleInfo(String userName);
}

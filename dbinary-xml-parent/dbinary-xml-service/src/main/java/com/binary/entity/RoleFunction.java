/**
 * 
 */
package com.binary.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author c_danyanxin
 * @date 2018年7月23日
 */
@Entity
@Table(name = "c_rolefunction")
public class RoleFunction implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6324231431446758642L;
	@Id
	  @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name="role_id",length=255)
	private Long roleId;
	@Column(name="function_id",length=255)
	private Long functionId;
	
}

package com.binary.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
//import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name = "c_user")
@SequenceGenerator(name = "seq_c_user", sequenceName = "seq_c_user")
public class User implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator="seq_c_user")
	private Long id;
	@Column(name="code",length=255)
	private String code;	
	@Column(name="password",length=255)
	private String password;
	@Column(name="name",length=255)
	private String name;
	@Column(name="status",length=255)
	private String status;
	@Column(name="address",length=255)
	private String address;
	
	@Column(name="job",length=255)
	private String job;
	@Column(name="age",length=255)
	private Long age;
	@Column(name="sex",length=255)
	private Long sex;
	
	@OneToMany(targetEntity = UserRole.class, mappedBy = "userId", fetch = FetchType.LAZY)
	private List<UserRole> userRoleList;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getJob() {
		return job;
	}
	public void setJob(String job) {
		this.job = job;
	}
	public Long getAge() {
		return age;
	}
	public void setAge(Long age) {
		this.age = age;
	}
	public Long getSex() {
		return sex;
	}
	public void setSex(Long sex) {
		this.sex = sex;
	}
	
	
	public List<UserRole> getUserRoleList() {
		return userRoleList;
	}
	public void setUserRoleList(List<UserRole> userRoleList) {
		this.userRoleList = userRoleList;
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", code=" + code + ", password=" + password + ", name=" + name + ", status=" + status
				+ ", address=" + address + ", job=" + job + ", age=" + age + ", sex=" + sex + ", userRoleList="
				+ userRoleList + "]";
	}
	
	
	
	
	
	
}

/**
 * 
 */
package com.binary.entity;

/**
 * @author c_danyanxin
 * @date 2018年8月10日
 */
public class FacadeFactory<T> {
	private T body;
	private String errorCode;
	private String errorMsg;
	public T getBody() {
		return body;
	}
	public void setBody(T body) {
		this.body = body;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	

}

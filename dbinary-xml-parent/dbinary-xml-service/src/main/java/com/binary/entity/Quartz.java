/**
 * 
 */
package com.binary.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author dbinary
 * @date 2018年7月21日 下午1:31:01
 */
@Entity
@Table(name = "c_quartz")
public class Quartz implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;  
    // 设置trigger名称  
	@Column(name="triggername")
    private String triggername;    
    //设置表达式  
	@Column(name="cronexpression")
    private String cronexpression;  
    // 设置Job名称  
	@Column(name="jobdetailname")
    private String jobdetailname;  
  //任务类名  
	@Column(name="targetobject")
    private String targetobject;  
    //类名对应的方法名  
	@Column(name="methodname")
    private String methodname;  
    //设置是否并发启动任务 0是false 非0是true 
	@Column(name="concurrent")
    private String concurrent;  
    // 如果计划任务不存则为1 存在则为0  
	@Column(name="state")
    private String state; 
	@Column(name="readme")
    private String readme;
	
	  //是否是已经存在的springBean 1是  0 否  
	@Column(name="isspringbean")
    private String isspringbean;  
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTriggername() {
		return triggername;
	}
	public void setTriggername(String triggername) {
		this.triggername = triggername;
	}
	public String getCronexpression() {
		return cronexpression;
	}
	public void setCronexpression(String cronexpression) {
		this.cronexpression = cronexpression;
	}
	public String getJobdetailname() {
		return jobdetailname;
	}
	public void setJobdetailname(String jobdetailname) {
		this.jobdetailname = jobdetailname;
	}
	public String getTargetobject() {
		return targetobject;
	}
	public void setTargetobject(String targetobject) {
		this.targetobject = targetobject;
	}
	public String getMethodname() {
		return methodname;
	}
	public void setMethodname(String methodname) {
		this.methodname = methodname;
	}
	public String getConcurrent() {
		return concurrent;
	}
	public void setConcurrent(String concurrent) {
		this.concurrent = concurrent;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getReadme() {
		return readme;
	}
	public void setReadme(String readme) {
		this.readme = readme;
	}
	public String getIsspringbean() {
		return isspringbean;
	}
	public void setIsspringbean(String isspringbean) {
		this.isspringbean = isspringbean;
	}  
	
	
	
	
	
	
	
	

}

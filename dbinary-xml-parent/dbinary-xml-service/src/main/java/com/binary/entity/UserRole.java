/**
 * 
 */
package com.binary.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author c_danyanxin
 * @date 2018年7月23日
 */
@Entity
@Table(name = "c_userrole")
public class UserRole implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3734558927797625094L;
	@Id
	  @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="user_id")
	private Long userId;
	@Column(name="role_id")
	private Long roleId;
	/*@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinColumn(name="role_id")
	private Role role;*/
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	@Override
	public String toString() {
		return "UserRole [id=" + id + ", userId=" + userId + ", role=" + roleId + "]";
	}

	
	

}



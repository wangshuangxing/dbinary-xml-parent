package com.binary.commons;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.util.StringUtils;

public class Pager implements Pageable{

	
	private int pageNo;
	private int pageSize;
	private String sort;
	private String desc;
	
	public Pager(int pageNo, int pageSize,String sort,
			String desc) {
		super();
		this.pageNo = pageNo;
		this.pageSize = pageSize;
		this.sort = sort;
		this.desc = desc;
		
	}
	
	public Pager(int pageNo, int pageSize, String sort) {
		super();
		this.pageNo = pageNo;
		this.pageSize = pageSize;
		this.sort = sort;
	}
	

	public Pager() {
		super();
	}
	

	@Override
	public int getPageNumber() {
		return this.pageNo;
	}
	@Override
	public int getPageSize() {
		return this.pageSize;
	}
	@Override
	public int getOffset() {
		return this.pageSize*(this.pageNo-1);
	}
	@Override
	public Sort getSort() {
		
		if(StringUtils.isEmpty(this.sort)){
			Direction dir = "desc".equals(this.desc)?Direction.DESC:Direction.ASC;
			return new Sort(dir,this.sort);
			
		}
		return null;
	}
	@Override
	public Pageable next() {
		return null;
	}
	@Override
	public Pageable previousOrFirst() {
		return null;
	}
	@Override
	public Pageable first() {
		return null;
	}
	@Override
	public boolean hasPrevious() {
		return false;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}
	
}

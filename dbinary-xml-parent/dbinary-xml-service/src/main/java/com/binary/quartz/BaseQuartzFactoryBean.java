/**
 * 
 */
package com.binary.quartz;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * @author dbinary
 * @date 2018年7月21日 上午11:08:02
 * function:basefactoryBean
 */

public class BaseQuartzFactoryBean implements ApplicationContextAware {
	
	public static ApplicationContext applicationContext;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		BaseQuartzFactoryBean.applicationContext = applicationContext;
				
	}
	public static  <T> T getBean(Class<T> clas){
		return applicationContext.getBean(clas);
	}
	
	public static Object getBeanName(String beanName){
		return applicationContext.getBean(beanName);
		
		
	}
	
	
	
}

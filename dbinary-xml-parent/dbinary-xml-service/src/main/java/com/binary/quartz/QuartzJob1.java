/**
 * 
 */
package com.binary.quartz;

import java.util.Date;

import org.springframework.stereotype.Component;

/**
 * @author dbinary
 * @date 2018年7月20日
 */
@Component
public class QuartzJob1 {
	
	public  void execute(){
		System.out.println(String.format("%tY-%<tm-%<td %<tH:%<tM:%<tS",new Date()));
	}
	public static void main(String[] args) {
//		execute();
	}
	

}

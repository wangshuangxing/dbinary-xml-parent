package com.binary.testproxy;
/**
 * test jdk动态的代理实现Aop功能:根据是否存在user给予权限调用方法
 * @author wangchangbo
 *
 */
public class UserServiceImpl implements UserService {

	private String user =null;
	
	public String getUser() {
		return user;
	}
	public UserServiceImpl(){}
	public UserServiceImpl(String user) {
		this.user = user;
	}

	@Override
	public void saveUser(Integer id, String name) {
		System.out.println("saveUser.............");
	}

	@Override
	public String getUserName(Integer id) {
		System.out.println("getUserName.............");
		return user;
	}

}

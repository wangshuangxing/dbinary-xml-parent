package com.binary.testproxy;

import org.junit.Test;

public class TestUserProxy {

	@Test
	public void testUserProxy(){
		ProxyFactory proxy = new ProxyFactory();
		UserService service =(UserService)proxy.createProxy(new UserServiceImpl(null));
		service.getUserName(1);
	}
	@Test public void test1(){
		CglibProxyFactory cglig = new CglibProxyFactory();
		UserServiceImpl impl = (UserServiceImpl)cglig.createProxy(new UserServiceImpl("1"));
		impl.getUserName(1);
	}
}

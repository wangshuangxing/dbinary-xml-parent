package com.binary.testproxy;

import java.lang.reflect.Method;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

/**
 * cglib
 * @author wangchangbo
 *
 */
public class CglibProxyFactory implements MethodInterceptor {
	private Object target;
	public Object createProxy(Object target){
		this.target = target;
		Enhancer hancer = new Enhancer();
		hancer.setSuperclass(target.getClass());
		hancer.setCallback(this);
		return hancer.create();
	}
	@Override
	public Object intercept(Object proxy, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
		
		UserServiceImpl impl = (UserServiceImpl)this.target;
		Object result = null;
		if(impl.getUser()!=null){
			result = methodProxy.invoke(target,args);
		}
		return result;
		
	}
}

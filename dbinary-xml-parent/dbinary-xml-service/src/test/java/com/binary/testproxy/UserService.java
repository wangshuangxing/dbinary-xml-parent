package com.binary.testproxy;

public interface UserService {

	void saveUser(Integer id,String name);
	String getUserName(Integer id);
	
}

package com.binary.test;




import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:springdata/spring-jpa.xml" })
public class TestBase extends AbstractJUnit4SpringContextTests {
	
	public <T> T getBean(Class<T> type){
		return applicationContext.getBean(type);
	}
	
	public Object getBeanName(String beanName){
		return applicationContext.getBean(beanName);
	}
	public ApplicationContext getApplicationContext(){
		return applicationContext;
	}
	@Test
	public void test1(){
		System.out.println("test testBase");
	}
}

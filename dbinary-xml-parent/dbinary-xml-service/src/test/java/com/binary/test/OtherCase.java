package com.binary.test;

import java.util.Scanner;

public class OtherCase {
	private final static String[] value = { "零", "壹", "贰", "叁", "肆", "伍", "陆", "柒", "捌", "玖" };

	private final static String[] money = { "分", "角", "元", "拾", "佰", "仟", "万", "亿" };

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		String str = scan.nextLine();
		/**
		 * 
		 * 判断有没有小数, 小数前有多少位; 超9位,为亿;
		 * 
		 */
		String[] num = str.split("\\.");

		long d = Long.parseLong(num[0]);

		StringBuilder sb = new StringBuilder();
		if (d > 100000000) {
			get(d, sb, money[7], 1000000000000L);

		}
		if (d > 10000) {
			get(d, sb, money[6], 100000000L);

		}

		get(d, sb, money[2], 10000);
		
		if (num.length == 2) {
		
			if (num[1].charAt(0) != '0') {
				appendNums(Long.parseLong(num[1].charAt(0) + ""), money[1], sb);

			}
			if (num[1].charAt(1) != '0') {
				appendNums(Long.parseLong(num[1].charAt(1) + ""), money[0], sb);
			}

		}
		System.out.println(sb);

	}

	private static void get(long d, StringBuilder sb, String string, long l) {
		
		getInfor(d, sb, money[5], l);
		
		getInfor(d, sb, money[4], l / 10);
		
		getInfor(d, sb, money[3], l / 100);
		
		getInfor(d, sb, "", l / 1000);
		
		sb.append(string);
	}

	private static void getInfor(long d, StringBuilder sb, String string, long l) {

		long j = d % l;
		long i = j / (l / 10);
		appendNums(i, string, sb);
	}

	private static void appendNums(long i, String string, StringBuilder sb) {
		if (i > 0) {
			String s = creatNum(i);
			sb.append(s);
			sb.append(string);
		}
	}

	private static String creatNum(long i) {

		return value[(int) i];
	}
}

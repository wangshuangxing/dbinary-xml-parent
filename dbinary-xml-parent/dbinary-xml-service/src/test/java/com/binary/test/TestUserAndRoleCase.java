package com.binary.test;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.alibaba.fastjson.JSON;
import com.binary.entity.Quartz;
import com.binary.entity.Role;
import com.binary.entity.User;
import com.binary.entity.UserRole;
import com.binary.repositroy.QuartzRepository;
import com.binary.repositroy.RoleRepository;
import com.binary.repositroy.UserRepository;
import com.binary.repositroy.UserRoleRepository;
import com.binary.service.UserService;

public class TestUserAndRoleCase extends TestBase {
	
	@Autowired
	private RoleRepository roleR;
	@Autowired
	private UserRepository userR;
	@Autowired
	private QuartzRepository quartzR;
	@Autowired
	private UserService userS;
	@Autowired
	private UserRoleRepository urRepository;
	
	@Test
	public  void dataSource1(){
		DriverManagerDataSource dataSource = (DriverManagerDataSource)getBeanName("driverManagerDataSource");
		System.out.println("datasource:"+dataSource.getUrl()+":url，"+dataSource.getUsername());
	}
	@Test
	public void testUserCase0(){
		User user = new User();
		user.setId(14l);
		user.setAge(13l);
		user.setJob("chengxuyuan");
		user.setName("quranboqulinxue");
		user.setSex(2l);
		user.setAddress("shandongsheng");
		user = userS.save(user);
		System.out.println(user);
	}
	@Test
	public void testUserCase1(){
		Page<Object[]> page = userR.findByUserName("");
		System.out.println(page.getNumber()+","+page.getSize()+","+page.getTotalPages()+","+"\n"+JSON.toJSONString(page.getContent()));
	}
	@Test
	public void testUserCase2(){
		Object user = userR.findByName("wangchangbo");
				System.out.println(JSON.toJSONString(user));
	}
	@Test
	public void testUserCase3(){
		List<User> users = userR.findAllEntityByName("c_wangchbo","山东省济宁市");
		System.out.println(JSON.toJSONString(users));
	}
	@Test
	public void testUserCase4(){
		int i  = userS.updateUserByName("山东省济宁市鱼台县", "c_wangchbo");
		System.out.println(i);
	}
	@Test
	public void testRoleCase(){
		List<Role> roles = roleR.findListByName("管理员");
		System.out.println(JSON.toJSONString(roles));
	}
	
	@Test
	public void testQuartz(){
		Iterable<Quartz> quartz = quartzR.findAll();
		System.out.println(JSON.toJSONString(quartz));
	}
	
/*	@Test
	public void testUserRole(){
		SessionFactory sf = null;
		Session session = sf.openSession();
		List<UserRole> userRole = urRepository.findByUserId(1l);
		System.out.println(JSON.toJSONString(userRole));
	}*/
	
}

/**
 * 
 */
package com.binary.redis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

/**
 * @author dbinary
 * @date 2018年8月1日
 * 接受者
 */
public class SubRedis {

	public static void main(String[] args) {
		Jedis jedis = new Jedis("localhost",6384);
		jedis.auth("123456");
		MyListener l = new MyListener();
		jedis.subscribe(l, "cctv");
	}
	private static class MyListener extends JedisPubSub{
		@Override
		public void onMessage(String channel,String message){
			System.out.println(channel+":"+message);
		}
	}
}

/**
 * 
 */
package com.binary.redis;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Transaction;

import com.binary.test.TestBase;

/**
 * @author dbinary
 * @date 2018年7月26日
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath*:redis/spring-redis.xml"})
public class redisTest  {
	@Resource(name="dataRedisClient")
	private DataRedisClient client;
	Jedis jedis = null;
	@Before
	public void before(){
	   jedis =client.getFactory().getJedis(3).getResource(); 
	}
	@Test
	public void test1(){//test 管道技术
		//test generation method
		System.out.println("插入redis技术");
		long l = System.currentTimeMillis();
		for(int i=0;i<500;i++){
			jedis.set(i+"key"+i, "me");
		}
		System.out.println("time:"+(System.currentTimeMillis()-l));
		
		
		//test redis pipe method
		System.out.println("管道技术");
		long ll = System.currentTimeMillis();
		Pipeline pipe = jedis.pipelined();
		for(int i=0;i<500;i++){
			pipe.set("key"+i, "me");
		}
		pipe.sync();
		System.out.println("redis pipe time:"+(System.currentTimeMillis()-ll));
	}
	@Test
	public void test2(){// test redis transation 
		
//		jedis.watch("money","out");
		try {
			Transaction t = jedis.multi();
			t.set("2", "two");
			int i = 100/0;
			
			t.set("3", "three");
			t.exec();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	@Test
	public void test3(){// test  watch redis transation   credit card
		jedis.set("money", "5000");
		jedis.set("debt", "1000");
		int out = 3000;
		Integer money = Integer.parseInt(jedis.get("money"));
		if(money-out<0){
			System.out.println("信用额度不够了，不能耍信用卡了");
			return;
		}else{
			jedis.watch("money","debt");
//			jedis.set("money", "6000");
			Transaction t = jedis.multi();
		
			t.incrBy("debt",out);
			t.decrBy("money", out);
			List<Object>list = t.exec();
			jedis.unwatch();
			if(list==null||list.isEmpty()){
				System.out.println("error:xinyongyue"+jedis.get("money")+",zaiwu"+jedis.get("debt"));
			}else{
				System.out.println("success 信用余额："+jedis.get("money")+", 债务余额："+jedis.get("debt"));
			}
		}
		} 
		
	}

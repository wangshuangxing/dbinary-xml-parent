/**
 * 
 */
package com.binary.redis;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import redis.clients.jedis.Jedis;

/**
 * @author c_danyanxin
 * @date 2018年8月1日
 */
public class Redis1 {
	public static void main(String[] args) {
		Jedis jedis = new Jedis("localhost",6384);
		jedis.auth("123456");
		jedis.flushAll();
		jedis.select(10);
//	1、	redis generation method
//		generationMethod(jedis);
//  2.redis string method
	//	jedisString(jedis);
//  3.redis hash method
		//getHashRedis(jedis);
//  4.redis list method	
		//redisList(jedis);
//  5.redis set method	
		jedis.sadd("set-1","name","age","peter");
//		jedis.srem("set-1","name");
		System.out.println(jedis.scard("set-1"));
		for(String key:jedis.smembers("set-1")){
			System.out.println(key);
		}
		jedis.sadd("set-2","nam","ag","peter");
		System.out.println(jedis.sinter("set-1","set-2"));
		jedis.sinterstore("set3", "set-1","set-2");
		System.out.println(jedis.get("set3"));
		jedis.close();
	}

	/**
	 * @param jedis
	 */
	private static void redisList(Jedis jedis) {
		jedis.lpush("list-1", "aa","bb","cc");//cc,bb,aa
		jedis.rpush("list-1", "dd","ee","ff");//cc,bb,aa dd,ee,ff
		System.out.println(jedis.llen("list-1"));
		System.out.println(jedis.lpop("list-1"));//bb,aa dd,ee,ff
		System.out.println(jedis.lindex("list-1", 2));
		System.out.println(jedis.lrange("list-1", 2, 4));
	}

	/**
	 * @param jedis
	 */
	private static void getHashRedis(Jedis jedis) {
		HashMap<String,String> hp = new HashMap<>();
		hp.put("name", "hp-pc");
		hp.put("price", "3500");
		jedis.hmset("hp", hp);
		System.out.println(jedis.hlen("hp"));
		
		jedis.hset("hp", "madein", "china");
		System.out.println(jedis.hlen("hp"));
		Map<String,String> map = jedis.hgetAll("hp");
		for(String key:map.keySet()){
			System.out.println(key+":"+map.get(key));
		}
		System.out.println(jedis.hget("hp","madein"));
		for(String key:jedis.hkeys("hp")){
			System.out.println(key+";"+jedis.hget("hp",key));
		}
		for(String value:jedis.hvals("hp")){
			System.out.println(value);
		}
	}

	/**
	 * @param jedis
	 */
	@SuppressWarnings("unused")
	private static void jedisString(Jedis jedis) {
		jedis.set("country","china");
		jedis.append("country", " amazing");
		System.out.println(jedis.get("country"));
		System.out.println(jedis.exists("country"));
		System.out.println(jedis.strlen("country"));
		System.out.println(jedis.get("country"));
		jedis.setrange("country", 6, "is a great");
		System.out.println(jedis.get("country"));
		System.out.println(jedis.getrange("country",0,5));
		jedis.set("age", "20");
		jedis.incr("age");
		System.out.println(jedis.get("age"));
		jedis.incrBy("age",10);
		System.out.println(jedis.get("age"));
		jedis.decr("age");
		System.out.println(jedis.get("age"));
		jedis.decrBy("age",10);
		System.out.println(jedis.get("age"));
		jedis.flushDB();
		jedis.mset("name","dbinary","age","29","address","shandong province");
		Set<String> list = jedis.keys("*");
		for(String str:list){
			System.out.println(str+":"+jedis.get(str));
		}
	}

	/**
	 * @param jedis
	 */
	@SuppressWarnings("unused")
	private static void generationMethod(Jedis jedis) {
		System.out.println(jedis.getDB());
		jedis.flushDB();
		jedis.flushAll();
		jedis.select(10);
		System.out.println(jedis.getDB());
		jedis.set("aa", "a");
		jedis.set("ab", "b");
		jedis.set("ac", "c");
		jedis.set("ad", "d");
		Set<String> list = jedis.keys("a*");
		for(String str:list){
			System.out.println(str+":"+jedis.get(str));
		}
		System.out.println(jedis.dbSize());
		jedis.move("aa", 1);
		jedis.select(10);
		System.out.println(jedis.dbSize());
		jedis.select(1);
		System.out.println(jedis.dbSize());
		jedis.expire("aa", 5);
		jedis.setex("bb",7,"6");
		try {
			System.out.println("sleep 6 seconds");
			Thread.sleep(6000);
			System.out.println(jedis.get("bb"));
			System.out.println("sleep 2 seconds");
			Thread.sleep(2000);
		} catch (InterruptedException e) {
		
			e.printStackTrace();
		}
		
		System.out.println(jedis.dbSize());
		
	}
}
